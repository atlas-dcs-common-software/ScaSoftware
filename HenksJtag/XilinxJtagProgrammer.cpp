/*
 * XilinxJtagProgrammer.cpp
 *
 *  Created : 18 Jun 2018
 *  Author  : pnikiel
 *  Extended: Dec 2019, henkb
 */

#include <HenksJtag/XilinxJtagProgrammer.h>
#include <HenksJtag/BitFile.h>

using namespace std;

namespace HenksJtag
{
// Xilinx VIRTEX7 JTAG instructions
#define V7_IRLEN        6
#define V7_IDCODE_LEN   32
#define V7_CFG_OUT      0x04
#define V7_CFG_IN       0x05
#define V7_IDCODE       0x09
#define V7_JPROGRAM     0x0B
#define V7_JSTART       0x0C
#define V7_JSHUTDOWN    0x0D
#define V7_ISC_ENABLE   0x10
#define V7_ISC_PROGRAM  0x11
#define V7_ISC_DISABLE  0x16
#define V7_BYPASS       0x3F

const string V7_STATBITS_STR[] = {
  "CRC ERROR                     ",
  "DECRYPTOR ENABLE              ",
  "PLL LOCK STATUS               ",
  "DCI MATCH STATUS              ",
  "END OF STARTUP (EOS) STATUS   ",
  "GTS_CFG_B STATUS              ",
  "GWE STATUS                    ",
  "GHIGH STATUS                  ",
  "MODE PIN M[0]                 ",
  "MODE PIN M[1]                 ",
  "MODE PIN M[2]                 ",
  "INIT_B INTERNAL SIGNAL STATUS ",
  "INIT_B PIN                    ",
  "DONE INTERNAL SIGNAL STATUS   ",
  "DONE PIN                      ",
  "IDCODE ERROR                  ",
  "SECURITY ERROR                ",
  "SYSTEM MON OVERTEMP ALARM STAT",
  "CFG STARTUP STATEMACHINE PHASE",
  "CFG STARTUP STATEMACHINE PHASE",
  "CFG STARTUP STATEMACHINE PHASE",
  "RESERVED                      ",
  "RESERVED                      ",
  "RESERVED                      ",
  "RESERVED                      ",
  "CFG BUS WIDTH DETECTION       ",
  "CFG BUS WIDTH DETECTION       ",
  "HMAC ERROR                    ",
  "PUDC_B PIN                    ",
  "BAD PACKET ERROR              ",
  "CFGBVS PIN                    ",
  "RESERVED                      "
};

// ----------------------------------------------------------------------------

XilinxJtagProgrammer::XilinxJtagProgrammer( Sca::Sca& sca,
                                            unsigned int freqMhz,
                                            unsigned int maxGroupSize )
  : m_jtagPort( sca, maxGroupSize ),
    m_freqMhz( freqMhz )
{
}

// ----------------------------------------------------------------------------

XilinxJtagProgrammer::~XilinxJtagProgrammer()
{
}

// ----------------------------------------------------------------------------

bool XilinxJtagProgrammer::programFromBitFile( const std::string& filename,
                                               int devices_before,
                                               int instr_bits_before,
                                               int devices_after,
                                               int instr_bits_after,
                                               int bypass_instruction )
{
  //! Will return false if the programming result was not OK,
  //! Will throw in case of an exceptional situation
  BitFile bitfile;
  try {
    bitfile.setFile( filename );
  }
  catch( std::runtime_error &e ) {
    LOG(Log::ERR) << filename << ": " << e.what();
    return false;
  }

  bool reverseBytes = true;
  if( reverseBytes )
    bitfile.reverseBytes();

  if( !bitfile.valid() )
    {
      LOG(Log::ERR) << "Bitfile invalid";
      return false;
    }

  try {
    if( (devices_before > 0 && instr_bits_before > 0) ||
        (devices_after > 0 && instr_bits_after > 0) )
      return this->program( bitfile, devices_before, instr_bits_before,
                            devices_after, instr_bits_after,
                            bypass_instruction );
    else
      return this->program( bitfile );
  }
  catch( std::runtime_error &e ) {
    LOG(Log::ERR) << e.what();
    return false;
  }
}

// ----------------------------------------------------------------------------

bool XilinxJtagProgrammer::programFromBlob( const std::string& blob,
                                            int devices_before,
                                            int instr_bits_before,
                                            int devices_after,
                                            int instr_bits_after,
                                            int bypass_instruction )
{
  //! Will return false if the programming result was not OK,
  //! Will throw in case of an exceptional situation
  BitFile bitfile;
  try {
    bitfile.setBlob( blob );
  }
  catch( std::runtime_error &e ) {
    LOG(Log::ERR) << "<blob>: " << e.what();
    return false;
  }

  bool reverseBytes = true;
  if( reverseBytes )
    bitfile.reverseBytes();

  if( !bitfile.valid() )
    {
      LOG(Log::ERR) << "Bitfile invalid";
      return false;
    }

  try {
    if( (devices_before > 0 && instr_bits_before > 0) ||
        (devices_after > 0 && instr_bits_after > 0) )
      return this->program( bitfile, devices_before, instr_bits_before,
                            devices_after, instr_bits_after,
                            bypass_instruction );
    else
      return this->program( bitfile );
  }
  catch( std::runtime_error &e ) {
    LOG(Log::ERR) << e.what();
    return false;
  }
}

// ----------------------------------------------------------------------------
/*
bool XilinxJtagProgrammer::program( BitFile& bitfile )
{
  m_jtagPort.configure( m_freqMhz );

  // Go to a defined state
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );

  m_jtagPort.showProgress( true );

  unsigned char instr;
  instr = V7_JPROGRAM;
  m_jtagPort.shiftIr( V7_IRLEN, &instr );
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );
  m_jtagPort.shift( JtagPort::RUN_TEST_IDLE, 10000*m_freqMhz );

  instr = V7_CFG_IN;
  m_jtagPort.shiftIr( V7_IRLEN, &instr );
  m_jtagPort.shiftDr( bitfile.nbytes()*8, bitfile.data() );

  m_jtagPort.showProgress( false );

  instr = V7_JSTART;
  m_jtagPort.shiftIr( V7_IRLEN, &instr );
  m_jtagPort.shift( JtagPort::RUN_TEST_IDLE, 2000*m_freqMhz );
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );

  m_jtagPort.clearReplyFrames();

  m_jtagPort.shift( JtagPort::SHIFT_IR, V7_IRLEN, nullptr, //read_tdi// true );
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );

  std::vector<Sca::Reply> replies = m_jtagPort.replyFrames();
  if( replies.size() != 1 )
    {
      std::ostringstream oss;
      oss << "Expected 1 reply, got " << replies.size();
      scasw_throw_runtime_error_with_origin( oss.str() );
    }
  bool result;
  if( replies[0][6] == 0x35 )
    {
      result = true; // Programming was successful
    }
  else
    {
      LOG(Log::ERR) << "FPGA programming likely failed, reply frame: "
                    << replies[0].toString();
      result = false;
    }
  return result;
}
*/
// ----------------------------------------------------------------------------

bool XilinxJtagProgrammer::program( BitFile& bitfile,
                                    int devices_before,
                                    int instr_bits_before,
                                    int devices_after,
                                    int instr_bits_after,
                                    int bypass_instruction )
{
  // Same function as program(bitfile), but now the FPGA is in a JTAG chain
  // with 'devices_before' JTAG devices *before* we reach the FPGA
  // and 'devices_after' JTAG devices *after* the FPGA,
  // before looping back to the JTAG master;
  // 'instr_bits_before' and 'instr_bits_after' are the total number of
  // instruction bits in those devices, before and after the FPGA resp.;
  // however, if 'bypass_instruction' is given it is taken as the BYPASS
  // instruction for each individual device (so the same in all devices..),
  // and in that case 'instr_bits_before' and 'instr_bits_after'
  // are the number of instruction bits of *one* device.

  m_jtagPort.configure( m_freqMhz );

  // Go to a defined state
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );

  m_jtagPort.showProgress( true );

  unsigned char instr;
  instr = V7_JPROGRAM;
  m_jtagPort.shiftIr( V7_IRLEN, instr, devices_before, instr_bits_before,
                      devices_after, instr_bits_after, bypass_instruction );
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );
  m_jtagPort.shift( JtagPort::RUN_TEST_IDLE, 10000*m_freqMhz );

  instr = V7_CFG_IN;
  m_jtagPort.shiftIr( V7_IRLEN, instr, devices_before, instr_bits_before,
                      devices_after, instr_bits_after, bypass_instruction );
  m_jtagPort.shiftDr( bitfile.nbytes()*8, bitfile.data(),
                      /*read_tdi*/ false, devices_before, devices_after );

  m_jtagPort.showProgress( false );

  instr = V7_JSTART;
  m_jtagPort.shiftIr( V7_IRLEN, instr, devices_before, instr_bits_before,
                      devices_after, instr_bits_after, bypass_instruction );
  m_jtagPort.shift( JtagPort::RUN_TEST_IDLE, 2000*m_freqMhz );
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );

  m_jtagPort.clearReplyFrames();

  m_jtagPort.shiftIr( V7_IRLEN, 0, devices_before, instr_bits_before,
                      devices_after, instr_bits_after,
                      bypass_instruction, /*read_tdi*/ true );
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );

  std::vector<Sca::Reply>& replies = m_jtagPort.replyFrames();

  // Expect a single reply
  // (if previous JTAG string <= 32 bits long, not including other devices)
  if( replies.size() != 1 )
    {
      std::ostringstream oss;
      oss << "Expected 1 reply, got " << replies.size();
      scasw_throw_runtime_error_with_origin( oss.str() );
    }
  bool result;
  if( replies[0][6] == 0x35 )
    {
      LOG(Log::INF) << "FPGA programming was successful!";
      result = true; // Programming was successful
    }
  else
    {
      LOG(Log::ERR) << "FPGA programming likely failed, reply frame: "
                    << replies[0].toString();
      result = false;
    }
  return result;
}

// ----------------------------------------------------------------------------
/*
uint32_t XilinxJtagProgrammer::readId()
{
  m_jtagPort.configure( m_freqMhz );

  unsigned char instr = V7_IDCODE;
  m_jtagPort.shiftIr( V7_IRLEN, &instr );
  m_jtagPort.clearReplyFrames();

  m_jtagPort.shiftDr( V7_IDCODE_LEN, //tdo// nullptr, //read_tdi// true );
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );

  std::vector<Sca::Reply> replies = m_jtagPort.replyFrames();

  // Expect a single reply (previous JTAG string <= 128 bits long)
  if( replies.size() != 1 )
    {
      std::ostringstream oss;
      oss << "Expected 1 reply, got " << replies.size();
      scasw_throw_runtime_error_with_origin( oss.str() );
    }

  // The ID is in the reply
  uint32_t code = ((replies[0][5] << 24) | (replies[0][4] << 16) |
                   (replies[0][7] << 8)  | (replies[0][6] << 0));
  return code;
}
*/
// ----------------------------------------------------------------------------

uint32_t XilinxJtagProgrammer::readId( int devices_before,
                                       int instr_bits_before,
                                       int devices_after,
                                       int instr_bits_after,
                                       int bypass_instruction )
{
  // Same function as readId(), but now the FPGA is in a JTAG chain
  // with 'devices_before' JTAG devices *before* we reach the FPGA
  // and 'devices_after' JTAG devices *after* the FPGA,
  // before looping back to the JTAG master;
  // 'instr_bits_before' and 'instr_bits_after' are the total number of
  // instruction bits in those devices, before and after the FPGA resp.;
  // however, if 'bypass_instruction' is given it is taken as the BYPASS
  // instruction for each individual device (so the same in all devices..),
  // and in that case 'instr_bits_before' and 'instr_bits_after'
  // are the number of instruction bits of *one* device.

  m_jtagPort.configure( m_freqMhz );

  unsigned char instr = V7_IDCODE;
  m_jtagPort.shiftIr( V7_IRLEN, instr, devices_before, instr_bits_before,
                      devices_after, instr_bits_after, bypass_instruction );

  m_jtagPort.clearReplyFrames();

  m_jtagPort.shiftDr( V7_IDCODE_LEN, /*tdo*/ nullptr, /*read_tdi*/ true,
                      devices_before, devices_after );
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );

  std::vector<Sca::Reply>& replies = m_jtagPort.replyFrames();

  // Expect a single reply
  // (if previous JTAG string <= 32 bits long, not including other devices)
  if( replies.size() != 1 )
    {
      std::ostringstream oss;
      oss << "Expected 1 reply, got " << replies.size();
      scasw_throw_runtime_error_with_origin( oss.str() );
    }

  // The ID is in the reply
  uint32_t code = ((replies[0][5] << 24) | (replies[0][4] << 16) |
                   (replies[0][7] << 8)  | (replies[0][6] << 0));
  return code;
}

// ----------------------------------------------------------------------------

std::string XilinxJtagProgrammer::idCodeToString( uint32_t id )
{
  std::stringstream output;
  output.fill('0');
  output.width(6);
  output << std::hex << id;
  // Xilinx 7-series FPGA family member IDs
  id = (id & 0x0FFFF000) >> 12;
  if( id == 0x3622 || id == 0x3620 || id == 0x37C4 ||
      id == 0x362F || id == 0x37C8 || id == 0x37C7 )
    output << " (Spartan-7)";
  else if( id == 0x37C3 || id == 0x362E || id == 0x37C2 ||
           id == 0x362D || id == 0x362C || id == 0x3632 ||
           id == 0x3631 || id == 0x3636 )
    output << " (Artix-7)";
  else if( id == 0x3647 || id == 0x364C || id == 0x3651 ||
           id == 0x3747 || id == 0x3656 || id == 0x3752 ||
           id == 0x3751 )
    output << " (Kintex-7)";
  else if( id == 0x3671 || id == 0x36B3 || id == 0x3667 ||
           id == 0x3682 || id == 0x3687 || id == 0x3692 ||
           id == 0x3691 || id == 0x3696 || id == 0x36D5 ||
           id == 0x36D9 || id == 0x36DB )
    output << " (Virtex-7)";
  else if( id == 0x0000 || id == 0xFFFF )
    output << " (no device connected?)";
  else
    output << " (<unknown>)";
  return output.str();
}

// ----------------------------------------------------------------------------
} /* namespace HenksJtag */
