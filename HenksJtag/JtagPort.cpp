/* Henk Boterenbrood is the original author of this file.
 * Brought to SCA-SW by Piotr Nikiel, June 2018.
 *
 * Nov 2019, Henk B: Major revision (see corresponding git commit messages).
 * Feb 2020, Henk B: Added return values to support JtagOperator;
 *                   use constants from include/Sca/Defs.h.
 * Aug 2020, Henk B: Added function shift(int, uint8_t*, uint8_t*, uint8_t*)
 *                   to support the Xilinx XVC protocol.
 * Jun 2021, Henk B: Added functions shiftIr/Dr( int, const uint8_t*,
 *                   std::vector<uint8_t>& ), with integrated TDI readback.
 */

#include <iostream>
#include <iomanip>
#include <thread>  // For this_thread::sleep_for
#include <sstream> // For ostringstream

#include <HenksJtag/JtagPort.h>

namespace HenksJtag
{
const int JTAG_WR_TDO[] = { ::Sca::Constants::Commands::JTAG_W_TDO0,
                            ::Sca::Constants::Commands::JTAG_W_TDO1,
                            ::Sca::Constants::Commands::JTAG_W_TDO2,
                            ::Sca::Constants::Commands::JTAG_W_TDO3 };
const int JTAG_RD_TDI[] = { ::Sca::Constants::Commands::JTAG_R_TDI0,
                            ::Sca::Constants::Commands::JTAG_R_TDI1,
                            ::Sca::Constants::Commands::JTAG_R_TDI2,
                            ::Sca::Constants::Commands::JTAG_R_TDI3 };
const int JTAG_WR_TMS[] = { ::Sca::Constants::Commands::JTAG_W_TMS0,
                            ::Sca::Constants::Commands::JTAG_W_TMS1,
                            ::Sca::Constants::Commands::JTAG_W_TMS2,
                            ::Sca::Constants::Commands::JTAG_W_TMS3 };

// ----------------------------------------------------------------------------

JtagPort::JtagPort( Sca::Sca& sca, unsigned int maxGroupSize )
  : _state( TEST_LOGIC_RESET ),
    _progress( false ),
    _ctrlReg( 0x0000 ),
    _goDelayFactor( 20 ), // Matches 1MHz JTAG clock
    _instr( 0 ),
    _instrLen( 0 ),
    _readTdi( true ),
    m_sca( sca ),
    m_synchronousService( sca.synchronousService() ),
    m_maxGroupSize( maxGroupSize )
{
  // Control Register bit 11 'MSB/LSB' (0x0800) here (above) initialised to 0;
  // GBT-SCA docu (v8.2-draft) says "bits transmitted msb to lsb" if 0,
  // but the opposite is true (Henk B, 23 Nov 2017)

  m_groupedRequests.reserve( 256 );
  m_keepReplies.reserve( 256 );
}

// ----------------------------------------------------------------------------

JtagPort::~JtagPort()
{
}

// ----------------------------------------------------------------------------

void JtagPort::configure( int freq_mhz )
{
  uint8_t data[4];

  // Configure GBT-SCA JTAG channel (standard; LEN=128 (represented by value 0)
  _ctrlReg &= ~0x007F;
  mapInt2Bytes( _ctrlReg, data );
  addScaJtagFrame( ::Sca::Constants::Commands::JTAG_W_CTRL, 4, data );
  // Frequency of 20MHz: f=2*10000000/(DIV+1) --> DIV=0
  // (NB: LSByte is in data[2] and MSByte in data[3])
  uint8_t div = 19; // Default 1MHz
  if( freq_mhz == 20 )
    div = 0;
  else if( freq_mhz == 10 )
    div = 1;
  else if( freq_mhz == 5 )
    div = 3;
  else if( freq_mhz == 4 )
    div = 4;
  else if( freq_mhz == 2 )
    div = 9;
  else if( freq_mhz == 1 )
    div = 19;
  else
    {
      //scasw_throw_runtime_error_with_origin("Invalid bitrate. Must be from the set {20,10,5,4,2,1} MHz");
      LOG(Log::WRN) << "Invalid MHz bitrate, must be from {20,10,5,4,2,1}; using 10";
      freq_mhz = 10;
      div = 1;
    }
  _goDelayFactor = 20/freq_mhz;
  memset( data, 0, 4 );
  data[2] = div;
  addScaJtagFrame( ::Sca::Constants::Commands::JTAG_W_FREQ, 4, data );

  uploadScaFrames();

  std::this_thread::sleep_for( std::chrono::milliseconds(1) );

  // Reset GBT-SCA JTAG TDO and TMS registers
  addResetTdoTms();

  uploadScaFrames();
}

// ----------------------------------------------------------------------------

void JtagPort::gotoState( state_t s )
{
  uint32_t tms;
  int      nbits = setState( s, &tms );
  if( nbits <= 0 ) return;

  // Configure the number of bits
  uint8_t data[4];
  _ctrlReg &= ~0x007F;
  _ctrlReg |= (nbits & 0x7F);
  mapInt2Bytes( _ctrlReg, data );
  addScaJtagFrame( ::Sca::Constants::Commands::JTAG_W_CTRL, 4, data );

  // Configure TDO and TMS
  memset( data, 0, 4 );
  addScaJtagFrame( ::Sca::Constants::Commands::JTAG_W_TDO0, 4, data );
  mapInt2Bytes( tms, data );
  addScaJtagFrame( ::Sca::Constants::Commands::JTAG_W_TMS0, 4, data );

  // Shift the bits (delay divided by 4: not more than 32 bits instead of 128)
  memset( data, 0, 4 );
  addScaJtagFrame( ::Sca::Constants::Commands::JTAG_GO, 2,
                   data, (_goDelayFactor*8)/4  );

  uploadScaFrames();
}

// ----------------------------------------------------------------------------

uint32_t JtagPort::shiftIr( int nbits, const uint8_t *tdo, bool read_tdi )
{
  return shift( SHIFT_IR, nbits, tdo, read_tdi );
}

// ----------------------------------------------------------------------------

uint32_t JtagPort::shiftIr( int nbits, const uint8_t *tdo,
                            std::vector<uint8_t> &tdi )
{
  uint32_t request_cnt = shift( SHIFT_IR, nbits, tdo, true );

  // Check the number of received TDI replies against expected number
  if( m_replyFrames.size() != request_cnt )
    {
      LOG(Log::ERR) << "shiftIr(): Expected " << request_cnt
                    << " replies, got " << m_replyFrames.size();
    }

  return extractTdi( tdi );
}

// ----------------------------------------------------------------------------

uint32_t JtagPort::shiftIr( int irlen,              int instr,
                            int devices_before,     int instr_bits_before,
                            int devices_after,      int instr_bits_after,
                            int bypass_instruction, bool read_tdi )
{
  bool final = false;
  if( devices_after > 0 && instr_bits_after > 0 )
    {
      // Insert BYPASS instruction for the devices present in the JTAG chain
      // *after* the selected device
      if( bypass_instruction > 0 )
        {
          // SPECIAL:
          // assume the BYPASS instruction is 'bypass_instruction'
          // for each device with instruction length 'instr_bits_after'
          std::vector<uint8_t> bypass( (devices_after*instr_bits_after+7)/8, 0x00 );
          int bit = 0;
          for( int dev=devices_after-1; dev>= 0; --dev )
            {
              for( int i=0; i<instr_bits_after; ++i, ++bit )
                if( bypass_instruction & (1<<i) )
                  bypass[bit/8] |= (1 << (bit & 0x7));
            }
          shift( SHIFT_IR, devices_after*instr_bits_after,
                 bypass.data(), false, final );
        }
      else
        {
          // Assume the BYPASS instruction is all 1-s
          // independent of individual instruction sizes
          // and 'instr_bits_after' is the *total* number
          // of instruction bits of the 'after-FPGA' devices
          std::vector<uint8_t> bypass( (instr_bits_after+7)/8, 0xFF );
          shift( SHIFT_IR, instr_bits_after, bypass.data(), false, final );
        }
    }

  // Shift instruction into the selected device
  final = (devices_before <= 0);
  uint8_t *tdo = (uint8_t *) &instr;
  uint32_t request_cnt = shift( SHIFT_IR, irlen, tdo, read_tdi, final );

  if( devices_before > 0 && instr_bits_before > 0 )
    {
      // Insert BYPASS instruction for the devices present in the JTAG chain
      // *before* the selected device
      final = true;
      if( bypass_instruction > 0 )
        {
          // SPECIAL:
          // assume the BYPASS instruction is 'bypass_instruction'
          // for each device with instruction length 'instr_bits_before'
          std::vector<uint8_t> bypass( (devices_before*instr_bits_before+7)/8, 0x00 );
          int bit = 0;
          for( int dev=devices_before-1; dev>= 0; --dev )
            {
              for( int i=0; i<instr_bits_before; ++i, ++bit )
                if( bypass_instruction & (1<<i) )
                  bypass[bit/8] |= (1 << (bit & 0x7));
            }
          shift( SHIFT_IR, devices_before*instr_bits_before,
                 bypass.data(), false, final );
        }
      else
        {
          // Assume the BYPASS instruction is all 1-s
          // independent of individual instruction sizes
          // and 'instr_bits_before' is the *total* number
          // of instruction bits of the 'before-FPGA' devices
          std::vector<uint8_t> bypass( (instr_bits_before+7)/8, 0xFF );
          shift( SHIFT_IR, instr_bits_before, bypass.data(), false, final );
        }
    }

  return request_cnt;
}

// ----------------------------------------------------------------------------

uint32_t JtagPort::shiftDr( int nbits, const uint8_t *tdo, bool read_tdi )
{
  return shift( SHIFT_DR, nbits, tdo, read_tdi );
}

// ----------------------------------------------------------------------------

uint32_t JtagPort::shiftDr( int nbits, const uint8_t *tdo,
                            std::vector<uint8_t> &tdi )
{
  uint32_t request_cnt = shift( SHIFT_DR, nbits, tdo, true );

  // Check the number of received TDI replies against expected number
  if( m_replyFrames.size() != request_cnt )
    {
      LOG(Log::ERR) << "shiftDr(): Expected " << request_cnt
                    << " replies, got " << m_replyFrames.size();
    }

  return extractTdi( tdi );
}

// ----------------------------------------------------------------------------

uint32_t JtagPort::shiftDr( int nbits, const uint8_t *tdo, bool read_tdi,
                            int devices_before, int devices_after )
{
  bool final = false;
  if( devices_after > 0 )
    {
      // BYPASS 1-bit per device for the devices *after* the selected device
      std::vector<uint8_t> bypass( (devices_after+7)/8, 0xFF );
      shift( SHIFT_DR, devices_after, bypass.data(), false, final );
    }

  // Shift data into the selected device(s)
  final = (devices_before <= 0);
  uint32_t request_cnt = shift( SHIFT_DR, nbits, tdo, read_tdi, final );

  if( devices_before > 0 )
    {
      // BYPASS 1-bit per device for the devices *before* the selected device
      std::vector<uint8_t> bypass( (devices_before+7)/8, 0xFF );
      final = true;
      shift( SHIFT_DR, devices_before, bypass.data(), false, final );
    }

  return request_cnt;
}

// ----------------------------------------------------------------------------

uint32_t JtagPort::shift( state_t state, int nbits, const uint8_t *tdo,
                          bool read_tdi, bool final )
{
  if( nbits <= 0 ) return 0;
  uint32_t request_cnt = 0;

  //if( state == SHIFT_IR || state == SHIFT_DR )
  gotoState( state );

  // Reset GBT-SCA JTAG TDO and TMS registers
  addResetTdoTms();

  uint8_t data[4];
  if( nbits > 128 )
    {
      // Configure the number of bits to shift: 128
      _ctrlReg &= ~0x007F;
      mapInt2Bytes( _ctrlReg, data );
      addScaJtagFrame( ::Sca::Constants::Commands::JTAG_W_CTRL, 4, data );
    }

  uploadScaFrames();

  int bytes_todo = nbits/8;
  int bytes_done = 0;
  int percentage = 0;
  if( _progress && state == SHIFT_DR )
    std::cout << "Uploading...     ";

  const uint8_t *ptdo = tdo;
  while( nbits > 128 )
    {
      // Accumulate up to 'max_cnt' groups of commands into a single DMA
      // (this is just to enable the display_percentage() below)
      int max_cnt = 50, cnt = 0;
      while( cnt < max_cnt && nbits > 128 )
        {
          if( tdo )
            {
              for( int i=0; i<4; ++i )
                {
                  data[2] = ptdo[0];
                  data[3] = ptdo[1];
                  data[0] = ptdo[2];
                  data[1] = ptdo[3];
                  addScaJtagFrame( JTAG_WR_TDO[i], 4, data );
                  ptdo += 4;
                }
            }

          // Shift the bits (delay required for 128 bits)
          memset( data, 0, 4 );
          addScaJtagFrame( ::Sca::Constants::Commands::JTAG_GO, 2,
                           data, _goDelayFactor*8 );

          if( read_tdi )
            {
              for( int i=0; i<4; ++i )
                // Delay of 1 us required !
                addScaJtagFrame( JTAG_RD_TDI[i], 0, data, 1, true );
              request_cnt += 4;
            }

          nbits -= 128;
          bytes_done += 16;
          ++cnt;
        }

      uploadScaFrames();

      if( _progress && state == SHIFT_DR )
        // Display percentage done
        displayPercentage( &percentage, bytes_done, bytes_todo );
    }

  // Final bits to shift (128 bits or less)
  _ctrlReg &= ~0x007F;
  if( nbits < 128 )
    _ctrlReg |= (nbits & 0x7F);
  mapInt2Bytes( _ctrlReg, data );
  addScaJtagFrame( ::Sca::Constants::Commands::JTAG_W_CTRL, 4, data );

  if( tdo )
    {
      for( int i=0; i<(nbits+31)/32; ++i )
        {
          data[2] = ptdo[0];
          data[3] = ptdo[1];
          data[0] = ptdo[2];
          data[1] = ptdo[3];
          addScaJtagFrame( JTAG_WR_TDO[i], 4, data );
          ptdo += 4;
        }
    }

  // Configure TMS: exit SHIFT_DR or SHIFT_IR state on the final bit;
  // all other states are done with TMS=0 only
  uint32_t tms = 0;
  if( final && (state == SHIFT_IR || state == SHIFT_DR) )
    tms = (1 << ((nbits-1) & 0x1F));
  if( tms != 0 )
    {
      mapInt2Bytes( tms, data );
      if( nbits <= 32 )
        addScaJtagFrame( JTAG_WR_TMS[0], 4, data );
      else if( nbits <= 64 )
        addScaJtagFrame( JTAG_WR_TMS[1], 4, data );
      else if( nbits <= 96 )
        addScaJtagFrame( JTAG_WR_TMS[2], 4, data );
      else
        addScaJtagFrame( JTAG_WR_TMS[3], 4, data );
    }

  // Shift the bits (add delay as required for 128 bits)
  memset( data, 0, 4 );
  addScaJtagFrame( ::Sca::Constants::Commands::JTAG_GO, 2,
                   data, _goDelayFactor*8 );

  if( read_tdi )
    {
      for( int i=0; i<(nbits+31)/32; ++i )
        {
          // Delay of 1 us required !
          addScaJtagFrame( JTAG_RD_TDI[i], 0, data, 1, true );
          ++request_cnt;
        }
    }

  uploadScaFrames();

  if( _progress && bytes_todo > 0 && state == SHIFT_DR )
    {
      bytes_done += (nbits+7)/8;
      // Display percentage done
      displayPercentage( &percentage, bytes_done, bytes_todo );
    }

  if( final && (state == SHIFT_IR || state == SHIFT_DR) )
    {
      // We left the SHIFT_DR or SHIFT_IR state
      if( state == SHIFT_DR )
        _state = EXIT1_DR;
      else
        _state = EXIT1_IR;
      // Continue to state RUN_TEST_IDLE
      gotoState( RUN_TEST_IDLE );
    }

  return request_cnt;
}

// ----------------------------------------------------------------------------

// Xilinx VIRTEX7 JTAG instructions
#define V7_CFG_OUT      0x04
#define V7_CFG_IN       0x05
#define V7_IDCODE       0x09
#define V7_JPROGRAM     0x0B
#define V7_JSTART       0x0C
#define V7_JSHUTDOWN    0x0D
#define V7_ISC_ENABLE   0x10
#define V7_ISC_PROGRAM  0x11
#define V7_ISC_DISABLE  0x16
#define V7_BYPASS       0x3F

uint32_t JtagPort::shift( int nbits,
                          const uint8_t *tms,
                          const uint8_t *tdo,
                          uint8_t *tdi )
{
  // This function has been added to support the Xilinx XVC protocol
  // (Xilinx Virtual Cable, basically remote low-level JTAG) whose main command
  // for transmitting data, is, according to the protocol description:
  // "shift:[number of bits][TMS vector][TDI vector]"
  // NB: TDI is the data shifted *out*, which we named *TDO* in this project!
  // At the same time TDI data is read out and stored in 'tdi',
  // although we take into account some special cases where we know TDI does not
  // contain meaningful information, so that we can skip reading TDI,
  // to improve performance (dramatically so, in these cases).
  //
  // While transmitting TMS+TDO bits we keep track of the current JTAG state,
  // also to be able to *disable* TDI read-back for performance reasons,
  // in particular when a Xilinx FPGA is being configured,
  // when TDI read-back is not significant.

  static const state_t JtagNextState[][2] =
    {
     [SELECT_DR_SCAN]   = {CAPTURE_DR, SELECT_IR_SCAN},
     [CAPTURE_DR]       = {SHIFT_DR, EXIT1_DR},
     [SHIFT_DR]         = {SHIFT_DR, EXIT1_DR},
     [EXIT1_DR]         = {PAUSE_DR, UPDATE_DR},
     [PAUSE_DR]         = {PAUSE_DR, EXIT2_DR},
     [EXIT2_DR]         = {SHIFT_DR, UPDATE_DR},
     [UPDATE_DR]        = {RUN_TEST_IDLE, SELECT_DR_SCAN},

     [TEST_LOGIC_RESET] = {RUN_TEST_IDLE, TEST_LOGIC_RESET},
     [RUN_TEST_IDLE]    = {RUN_TEST_IDLE, SELECT_DR_SCAN},

     [SELECT_IR_SCAN]   = {CAPTURE_IR, TEST_LOGIC_RESET},
     [CAPTURE_IR]       = {SHIFT_IR, EXIT1_IR},
     [SHIFT_IR]         = {SHIFT_IR, EXIT1_IR},
     [EXIT1_IR]         = {PAUSE_IR, UPDATE_IR},
     [PAUSE_IR]         = {PAUSE_IR, EXIT2_IR},
     [EXIT2_IR]         = {SHIFT_IR, UPDATE_IR},
     [UPDATE_IR]        = {RUN_TEST_IDLE, SELECT_DR_SCAN}
    };

  if( nbits <= 0 ) return 0;

  uint32_t request_cnt = 0;

  // Keep track of the JTAG state for this sequence of TMS bits,
  // and at the same time extract the JTAG instruction given
  // (NB: *entering* state SHIFT_IR does not carry the first instruction bit,
  //      while *leaving the state carries the last instruction bit,
  //      so in the loop below the *next* state is determined only at the end
  //      of the loop);
  // configure _readTdi accordingly, i.e. no need to read out TDI bits
  // when uploading a Xilinx FPGA configuration -> performance!
  int i, bit;
  //int cnt = 0;
  bool in_runtestidle    = false;
  bool in_shiftdr        = false;
  bool state_not_changed = true;
  int  rti_cnt           = 0;
  for( i=0; i<nbits; ++i )
    {
      if( _state == CAPTURE_IR )
        {
          // Going to receive an instruction: initialize
          _instr    = 0x00000000;
          _instrLen = 0;
        }
      else if( _state == SHIFT_IR )
        {
          // Extract and add an instruction bit
          bit = ((tdo[i/8] & (1 << (i&7))) != 0) ? 1 : 0;
          if( bit == 1 )
            _instr |= (1 << _instrLen);
          ++_instrLen;
        }
      else if( _state == UPDATE_IR )
        {
          LOG(Log::DBG) << "shift() IR=0x" << std::hex << _instr
                        << std::dec << " len=" << _instrLen;

          if( _instr == V7_CFG_IN )
            {
              // Going to execute JTAG instruction '5': disable TDI reading
              _readTdi = false;
              LOG(Log::DBG) << "shift() TDI -> OFF"; 
            }
        }
      else if( _state == UPDATE_DR )
        {
          if( _instr == V7_CFG_IN )
            {
              // Leaving state in which JTAG instruction '5' was executed:
              // re-enable TDI reading
              _readTdi = true;
              LOG(Log::DBG) << "shift() TDI -> ON"; 
            }
        }

      // Next JTAG state
      bit = ((tms[i/8] & (1 << (i&7))) != 0) ? 1 : 0;

      // DEBUG: display state transitions
      /*
      if( i == 0 )
        {
          std::cout << "States: " << JtagNextState[_state][bit];
          cnt = 1;
        }
      else
        {
          if( JtagNextState[_state][bit] != _state )
            {
              if( cnt > 1 )
                // Previous state occurred multiple times sequentially: display
                std::cout << "(" << std::dec << cnt << ") " << std::hex;
              else
                std::cout << " ";
              std::cout << JtagNextState[_state][bit];
              cnt = 1;
            }
          else
            {
              // Keep track of a state that is maintained multiple clock ticks
              // without displaying each individually: keep count
              ++cnt;
            }
        }
      */
      // Keep track of consecutive RUN_TEST_IDLE states
      // from the beginning of this sequence
      if( rti_cnt == i && _state == RUN_TEST_IDLE )
        ++rti_cnt;

      if( state_not_changed )
        state_not_changed = (_state == JtagNextState[_state][bit]);

      _state = JtagNextState[_state][bit];
    }
  /*
  // DEBUG: display state transitions
  if( cnt > 1 )
    // Current state occurred multiple times sequentially
    std::cout << "(" << std::dec << cnt << ") " << std::hex;
  std::cout << std::flush;
  */

  // Remaining in state RUN_TEST_IDLE in this call ?
  // Then there is most likely no need to read out TDI bits;
  // it also means all TMS bits in tms[] are zero,
  // so there is also no need to write the TMS registers more than once
  // (This is being introduced here to improve the performance
  //  of Vivado memory device programming, 9 Jan 2021)
  if( _state == RUN_TEST_IDLE && state_not_changed )
    in_runtestidle = true;

  // Remaining in state RUN_TEST_IDLE for some time ?
  if( in_runtestidle )
    // Already in permanent state RUN_TEST_IDLE
    rti_cnt = 0;
  else
    // The number of times we can skip reading TDI
    rti_cnt /= 128;

  // Remaining in state SHIFT_DR in this call ?
  // it means all TMS bits in tms[] are zero,
  // so no need to write them more than once
  if( _state == SHIFT_DR && state_not_changed )
    in_shiftdr = true;

  clearReplyFrames();

  uint8_t zeros[16];
  uint8_t data[4];
  if( nbits > 128 )
    {
      // Configure the number of bits to shift: 128
      _ctrlReg &= ~0x007F;
      mapInt2Bytes( _ctrlReg, data );
      addScaJtagFrame( ::Sca::Constants::Commands::JTAG_W_CTRL, 4, data );
      // For use in while-loop below
      memset( zeros, 0, 16 );
    }

  uploadScaFrames();

  const uint8_t *ptms = tms;
  const uint8_t *ptdo = tdo;
  uint8_t       *ptdi = tdi;
  int      nbytes = (nbits + 7) / 8;
  bool     write_tms = true;
  bool     tms_was_zero = false;
  bool     tms_is_zero;
  //bool   tdo_was_zero = false;
  //bool   tdo_is_zero;
  while( nbits > 128 )
    {
      //tdo_is_zero = (memcmp( ptdo, zeros, 16 ) == 0);
      if( write_tms )
        tms_is_zero = (memcmp( ptms, zeros, 16 ) == 0);
      for( int i=0; i<4; ++i )
        {
          if( write_tms )
            {
              if( tms_is_zero == false || tms_was_zero == false )
                {
                  data[2] = ptms[0];
                  data[3] = ptms[1];
                  data[0] = ptms[2];
                  data[1] = ptms[3];
                  addScaJtagFrame( JTAG_WR_TMS[i], 4, data );
                }
              ptms += 4;
            }

          // Don't set TDO registers unnecessarily, if all zeroes are written
          // and already previously was set to all zeroes
          // ###DOES NOT WORK? WHY? (if it works for TMS.. to be investigated)
          //if( tdo_is_zero == false || tdo_was_zero == false )
            {
              data[2] = ptdo[0];
              data[3] = ptdo[1];
              data[0] = ptdo[2];
              data[1] = ptdo[3];
              addScaJtagFrame( JTAG_WR_TDO[i], 4, data );
            }
          ptdo += 4;
        }
      tms_was_zero = tms_is_zero;
      //tdo_was_zero = tdo_is_zero;

      // If remaining in RUN_TEST_IDLE or SHIFT_DR there is no need
      // to write TMS registers more than once
      if( in_runtestidle || in_shiftdr )
        write_tms = false;

      // Shift the bits (add delay as required for 128 bits)
      memset( data, 0, 4 );
      addScaJtagFrame( ::Sca::Constants::Commands::JTAG_GO, 2,
                       data, _goDelayFactor*8 );

      if( rti_cnt > 0 )
        {
          // Skip TDI readout because we remain in state RUN_TEST_IDLE
          // (fill TDI return bytes with 0xFF)
          memset( ptdi, 0xFF, 16 );
          ptdi += 16;
          --rti_cnt;
        }
      else if( _readTdi && !in_runtestidle )
        {
          // Read out TDI registers
          for( int i=0; i<4; ++i )
            // Delay of 1 us required !
            addScaJtagFrame( JTAG_RD_TDI[i], 0, data, 1, true );
          request_cnt += 4;
        }

      nbits -= 128;
    }
  uploadScaFrames();

  // Final bits to shift (128 bits or less)
  _ctrlReg &= ~0x007F;
  if( nbits < 128 )
    _ctrlReg |= (nbits & 0x7F);
  mapInt2Bytes( _ctrlReg, data );
  addScaJtagFrame( ::Sca::Constants::Commands::JTAG_W_CTRL, 4, data );

  for( int i=0; i<4; ++i )
    if( nbits > i*32 )
      {
        if( write_tms )
          {
            data[2] = ptms[0];
            data[3] = ptms[1];
            data[0] = ptms[2];
            data[1] = ptms[3];
            addScaJtagFrame( JTAG_WR_TMS[i], 4, data );
            ptms += 4;
          }

        data[2] = ptdo[0];
        data[3] = ptdo[1];
        data[0] = ptdo[2];
        data[1] = ptdo[3];
        addScaJtagFrame( JTAG_WR_TDO[i], 4, data );
        ptdo += 4;
      }

  // Shift the bits (add delay as required for 128 bits)
  memset( data, 0, 4 );
  addScaJtagFrame( ::Sca::Constants::Commands::JTAG_GO, 2,
                   data, _goDelayFactor*8 );

  if( _readTdi && !in_runtestidle )
    {
      for( int i=0; i<(nbits+31)/32; ++i )
        {
          // Read out TDI register; delay of 1 us required !
          addScaJtagFrame( JTAG_RD_TDI[i], 0, data, 1, true );
          ++request_cnt;
        }
    }

  uploadScaFrames();

  // Check the number of received TDI replies against expected number
  if( m_replyFrames.size() != request_cnt )
    {
      LOG(Log::ERR) << "shift(): Expected " << request_cnt
                    << " replies, got " << m_replyFrames.size();
    }

  // Extract the TDI bits and store them in the proper order
  // (Note that the number of bits is rounded up to the next multiple of 32;
  //  the user needs to be aware of this, especially when he requests
  //  replies from multiple string uploads in one sequence)
  for( Sca::Reply &r: m_replyFrames )
    {
      if( r.channelId() != Sca::Constants::ChannelIds::JTAG ||
          r.error() != 0 ) // If error, exception thrown by JtagPort
        // Report or throw exception ?
        // (or let the user decide whether he receives sufficient bits)
        continue;

      if( r.size() < 8 )
        {
          // We expect only replies with TDI register content
          // so this is an unexpected reply, display it
          std::ostringstream oss;
          oss << std::hex << std::setfill('0');
          for( unsigned int i=0; i<r.size(); ++i )
            oss << ' ' << std::setw(2) << (uint32_t) r[i];
          // NB: If Payload::begin()/end() would exist, could write:
          //for( auto &byt: r )
          //  oss << ' ' << std::setw(2) << (uint32_t) byt;
          LOG(Log::ERR) << "shift(): Unexpected reply, size " << r.size()
                        << ":" << oss.str();
          continue;
        }

      // Data starts at index 4 and little-endian byte mapping is: 2,3,0,1;
      // copy data bytes into the destination array in this order
      ptdi[0] = r[4+2];
      ptdi[1] = r[4+3];
      ptdi[2] = r[4+0];
      ptdi[3] = r[4+1];
      ptdi += 4;
    }

  if( _readTdi )
    {
      if( in_runtestidle )
        {
          // TDI was not read out because of being in state RUN_TEST_IDLE:
          // instead fill tdi[] bytes directly with value 0xFF
          // (during Vivado memory device programming it was seen
          //  that TDI returns only(?) bits with value 1)
          memset( tdi, 0xFF, nbytes );
          LOG(Log::DBG) << "shift(): return " << nbytes << " TDI bytes 0xFF";
        }
      else
        {
          // TDI was read out
          LOG(Log::DBG) << "shift(): return "
                        << m_replyFrames.size()*4 << " TDI bytes";
        }
    }
  else
    {
      // TDI was not read out (fxvcserver returns bytes 0x00)
      LOG(Log::DBG) << "shift(): return " << nbytes << " TDI bytes empty";
    }

  LOG(Log::DBG) << "shift(): state = " << _state;

  // Represents the number of returned bytes with TDI bits
  // (NB: rounded to next multiple of 4 bytes)
  return nbytes;
}

// ----------------------------------------------------------------------------

int JtagPort::setState( state_t s, uint32_t *tms )
{
  // Do nothing for an invalid end state
  if( s > UPDATE_IR )
    return 0;

  int nbits = 0;
  *tms = 0x0;

  // Reset (from any state in at most 5 clock cycles with TMS=1)
  if( s == TEST_LOGIC_RESET )
    {
      *tms = 0x1F;
      _state = TEST_LOGIC_RESET;
      nbits = 5;
    }

  // Go to the requested TAP state step by step,
  // using the shortest possible path in the state diagram
  while( _state != s )
    {
      if( _state == TEST_LOGIC_RESET && _state != s )
        {
          // TMS=0
          ++nbits; 
          _state = RUN_TEST_IDLE;
        }
      if( _state == RUN_TEST_IDLE && _state != s )
        {
          *tms |= (1 << nbits); // TMS=1
          ++nbits; 
          _state = SELECT_DR_SCAN;
        }
      if( _state == SELECT_DR_SCAN && _state != s )
        {
          if( s < TEST_LOGIC_RESET )
            {
              // TMS=0
              _state = CAPTURE_DR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = SELECT_IR_SCAN;
            }
          ++nbits;
        }
      if( _state == CAPTURE_DR && _state != s )
        {
          if( s == SHIFT_DR )
            {
              // TMS=0
              _state = SHIFT_DR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = EXIT1_DR;
            }
          ++nbits;
        }
      if( _state == SHIFT_DR && _state != s )
        {
          *tms |= (1 << nbits); // TMS=1
          ++nbits;
          _state = EXIT1_DR;
        }
      if( _state == EXIT1_DR && _state != s )
        {
          if( s == PAUSE_DR ||
              s == EXIT2_DR )
            {
              // TMS=0
              _state = PAUSE_DR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = UPDATE_DR;
            }
          ++nbits;
        }
      if( _state == PAUSE_DR && _state != s )
        {
          *tms |= (1 << nbits); // TMS=1
          ++nbits;
          _state = EXIT2_DR;
        }
      if( _state == EXIT2_DR && _state != s )
        {
          if( s == SHIFT_DR ||
              s == EXIT1_DR ||
              s == PAUSE_DR )
            {
              // TMS=0
              _state = SHIFT_DR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = UPDATE_DR;
            }
          ++nbits;
        }
      if( _state == UPDATE_DR && _state != s )
        {
          if( s == RUN_TEST_IDLE )
            {
              // TMS=0
              _state = RUN_TEST_IDLE;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = SELECT_DR_SCAN;
            }
          ++nbits;
        }
      if( _state == SELECT_IR_SCAN && _state != s )
        {
          if( s != TEST_LOGIC_RESET )
            {
              // TMS=0
              _state = CAPTURE_IR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = TEST_LOGIC_RESET;
            }
          ++nbits;
        }
      if( _state == CAPTURE_IR && _state != s )
        {
          if( s == SHIFT_IR )
            {
              // TMS=0
              _state = SHIFT_IR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = EXIT1_IR;
            }
          ++nbits;
        }
      if( _state == SHIFT_IR && _state != s )
        {
          *tms |= (1 << nbits); // TMS=1
          ++nbits;
          _state = EXIT1_IR;
        }
      if( _state == EXIT1_IR && _state != s )
        {
          if( s == PAUSE_IR ||
              s == EXIT2_IR )
            {
              // TMS=0
              _state = PAUSE_IR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = UPDATE_IR;
            }
          ++nbits;
        }
      if( _state == PAUSE_IR && _state != s )
        {
          *tms |= (1 << nbits); // TMS=1
          ++nbits; 
          _state = EXIT2_IR;
        }
      if( _state == EXIT2_IR && _state != s )
        {
          if( s == SHIFT_IR  ||
              s == EXIT1_IR  ||
              s == PAUSE_IR )
            {
              // TMS=0
              _state = SHIFT_IR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = UPDATE_IR;
            }
          ++nbits;
        }
      if( _state == UPDATE_IR && _state != s )
        {
          if( s == RUN_TEST_IDLE )
            {
              // TMS=0
              _state = RUN_TEST_IDLE;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = SELECT_DR_SCAN;
            }
          ++nbits;
        }
    }
  return nbits;
}

// ----------------------------------------------------------------------------

void JtagPort::addResetTdoTms()
{
  // Reset GBT-SCA JTAG TDO and TMS registers
  uint8_t data[4] = { 0, 0, 0, 0 };
  addScaJtagFrame( JTAG_WR_TDO[0], 4, data );
  addScaJtagFrame( JTAG_WR_TDO[1], 4, data );
  addScaJtagFrame( JTAG_WR_TDO[2], 4, data );
  addScaJtagFrame( JTAG_WR_TDO[3], 4, data );
  addScaJtagFrame( JTAG_WR_TMS[0], 4, data );
  addScaJtagFrame( JTAG_WR_TMS[1], 4, data );
  addScaJtagFrame( JTAG_WR_TMS[2], 4, data );
  addScaJtagFrame( JTAG_WR_TMS[3], 4, data );
}

// ----------------------------------------------------------------------------

void JtagPort::mapInt2Bytes( uint32_t i, uint8_t *bytes )
{
  bytes[2] = (uint8_t) ((i >>  0) & 0xFF);
  bytes[3] = (uint8_t) ((i >>  8) & 0xFF);
  bytes[0] = (uint8_t) ((i >> 16) & 0xFF);
  bytes[1] = (uint8_t) ((i >> 24) & 0xFF);
}

// ----------------------------------------------------------------------------

void JtagPort::displayPercentage( int *percentage,
                                  int  bytes_done,
                                  int  bytes_todo )
{
  // Display percentage done (on change of integer value)
  int p = *percentage;
  double pf = ((double) 100.0*bytes_done)/(double) bytes_todo;
  if( (int) pf != p )
    {
      p = (int) pf;
      // Overwrite currently displayed percentage value
      std::cout << "\b\b\b\b\b    \b\b\b\b" << std::setw(3) << p << "% ";
      std::cout.flush();
      *percentage = p;
    }
}

// ----------------------------------------------------------------------------

void JtagPort::addScaJtagFrame( int cmd, int len, uint8_t *data,
                                unsigned int delay_us, bool keep_reply )
{
  Sca::Request request( ::Sca::Constants::ChannelIds::JTAG, cmd, data, len );
  request.setTrailingPause( delay_us );
  m_groupedRequests.push_back( request );
  m_keepReplies.push_back( keep_reply );

  if( delay_us > 0 )
    LOG(Log::DBG) << "Pushed:" << request.toString() << " delay=" << delay_us;
  else
    LOG(Log::DBG) << "Pushed:" << request.toString();

  if( m_groupedRequests.size() >= m_maxGroupSize )
    uploadScaFrames();
}

// ----------------------------------------------------------------------------

void JtagPort::uploadScaFrames_Old()
{
  if( m_groupedRequests.size() > 0 )
    {
      LOG(Log::DBG) << "Flushing " << m_groupedRequests.size() << " frames";

      // With JTAG rate dependent time-out
      std::vector<Sca::Reply> replies =
        m_synchronousService.sendAndWaitReply( m_groupedRequests,
                                               1000*_goDelayFactor );

      for( Sca::Reply &r: replies )
        throwIfScaReplyError( r );

      m_groupedRequests.clear();
      m_groupedRequests.reserve( 256 );
    }
}

// ----------------------------------------------------------------------------

void JtagPort::uploadScaFrames()
{
  // Upload accumulated frames with selective (TDI bits) read-back,
  // as indicated in m_keepReplies[]
  // (assumes the replies are returned in the same order as the requests)
  if( m_groupedRequests.size() > 0 )
    {
      LOG(Log::DBG) << "Uploading " << m_groupedRequests.size() << " frames";

      // With JTAG rate dependent time-out
      std::vector<Sca::Reply> replies =
        m_synchronousService.sendAndWaitReply( m_groupedRequests,
                                               1000*_goDelayFactor );

      // Check and select requested replies only
      int index = 0;
      for( Sca::Reply &r: replies )
        {
          throwIfScaReplyError( r );
          if( m_keepReplies[index] )
            m_replyFrames.push_back( r );
          ++index;
        }

      m_groupedRequests.clear();
      m_keepReplies.clear();
    }
}

// ----------------------------------------------------------------------------

uint32_t JtagPort::extractTdi( std::vector<uint8_t> &tdi )
{
  // Extract the TDI bits from the replies and store them in the proper order
  // (Note that the number of bits is rounded up to the next multiple of 32;
  //  the user needs to be aware of this, especially when he requests
  //  replies from multiple string uploads in one sequence!)
  uint32_t nbytes = 0;
  for( Sca::Reply &r: m_replyFrames )
    {
      if( r.channelId() != Sca::Constants::ChannelIds::JTAG ||
          r.error() != 0 ) // If error, exception thrown by JtagPort
        // Report or throw exception ?
        // (or let the user decide whether he receives sufficient bits)
        continue;

      if( r.size() < 8 )
        {
          // We expect only replies with TDI register content
          // so this is an unexpected reply, display it
          std::ostringstream oss;
          oss << std::hex << std::setfill('0');
          for( unsigned int i=0; i<r.size(); ++i )
            oss << ' ' << std::setw(2) << (uint32_t) r[i];
          // NB: If Payload::begin()/end() would exist, could write:
          //for( auto &byt: r )
          //  oss << ' ' << std::setw(2) << (uint32_t) byt;
          LOG(Log::ERR) << "extractTdi(): Unexpected reply, size " << r.size()
                        << ":" << oss.str();
          continue;
        }

      // Data starts at index 4 and little-endian byte mapping is: 2,3,0,1;
      // copy data bytes into the destination array in this order
      tdi.push_back( r[4+2] );
      tdi.push_back( r[4+3] );
      tdi.push_back( r[4+0] );
      tdi.push_back( r[4+1] );
      nbytes += 4;
    }
  return nbytes;
}

// ----------------------------------------------------------------------------
} // namespace HenksJtag
