/*
 * JtagOperator.cpp
 *
 *  Created : 29 Jan 2020
 *  Author  : henkb
 */

#include <HenksJtag/JtagOperator.h>

using namespace std;

namespace HenksJtag
{
// ----------------------------------------------------------------------------

JtagOperator::JtagOperator( Sca::Sca& sca,
                            unsigned int freqMhz,
                            unsigned int maxGroupSize )
  : m_jtagPort( sca, maxGroupSize )
{
  m_jtagPort.configure( freqMhz );
}

// ----------------------------------------------------------------------------

JtagOperator::~JtagOperator()
{
}

// ----------------------------------------------------------------------------

bool JtagOperator::setFile( std::string filename )
{
  try {
    m_jtagSequence.setFile( filename );
  }
  catch( std::runtime_error &e ) {
    LOG(Log::ERR) << "JtagOperator::setFile(): " << e.what();
  }
  return m_jtagSequence.isValid();
}

// ----------------------------------------------------------------------------

bool JtagOperator::addString( std::vector<unsigned char> &data,
                              unsigned int nbits, bool instr, bool read )
{
  return m_jtagSequence.addString( data, nbits, instr, read );
}
    
// ----------------------------------------------------------------------------

bool JtagOperator::hasValidSequence( )
{
  return m_jtagSequence.isValid();
}

// ----------------------------------------------------------------------------

void JtagOperator::clearSequence( )
{
  m_jtagSequence.clear();
}

// ----------------------------------------------------------------------------

unsigned int JtagOperator::sequenceSize( ) const
{
  return m_jtagSequence.size();
}

// ----------------------------------------------------------------------------

bool JtagOperator::uploadSequence( )
{
  if( !m_jtagSequence.isValid() )
    return false;
  
  m_jtagPort.clearReplyFrames();

  // Go to a defined state
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );

  // Upload the JTAG strings, either in SHIFT-IR or in SHIFT-DR state, 1-by-1
  m_replyBytes.clear();
  for( unsigned int i=0; i<m_jtagSequence.size(); ++i )
    {
      bool reversed       = false;
      int  nbits          = m_jtagSequence.bitCount( i );
      unsigned char *bits = m_jtagSequence.bits( i, reversed );
      if( m_jtagSequence.isInstruction(i) )
        {
          LOG(Log::TRC) << "Instr " << nbits;
          if( m_jtagSequence.readTdi(i) )
            m_jtagPort.shiftIr( nbits, bits );
          else
            m_jtagPort.shiftIr( nbits, bits, m_replyBytes );
        }
      else
        {
          LOG(Log::TRC) << "Data " << nbits;
          if( m_jtagSequence.readTdi(i) )
            m_jtagPort.shiftDr( nbits, bits );
          else
            m_jtagPort.shiftDr( nbits, bits, m_replyBytes );
        }
      // Cycle through state RUN_TEST_IDLE ?
      //m_jtagPort.gotoState( JtagPort::RUN_TEST_IDLE );
    }

  return true;
}

// ----------------------------------------------------------------------------

bool JtagOperator::uploadSequence_old( )
{
  if( !m_jtagSequence.isValid() )
    return false;
  
  m_jtagPort.clearReplyFrames();

  // Go to a defined state
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );

  // Upload the JTAG strings, either in SHIFT-IR or in SHIFT-DR state, 1-by-1
  unsigned int expected_replies = 0;
  for( unsigned int i=0; i<m_jtagSequence.size(); ++i )
    {
      bool reversed       = false;
      int  nbits          = m_jtagSequence.bitCount( i );
      unsigned char *bits = m_jtagSequence.bits( i, reversed );
      if( m_jtagSequence.isInstruction(i) )
        {
          LOG(Log::TRC) << "Instr " << nbits;
          expected_replies +=
            m_jtagPort.shiftIr( nbits, bits, m_jtagSequence.readTdi(i) );
        }
      else
        {
          LOG(Log::TRC) << "Data " << nbits;
          expected_replies +=
            m_jtagPort.shiftDr( nbits, bits, m_jtagSequence.readTdi(i) );
        }
      // Cycle through state RUN_TEST_IDLE ?
      //m_jtagPort.gotoState( JtagPort::RUN_TEST_IDLE );
    }

  // Get the replies containing the requested return TDI bits
  std::vector<Sca::Reply>& replies = m_jtagPort.replyFrames();

  // Check against expected
  if( replies.size() != expected_replies )
    {
      std::ostringstream oss;
      oss << "JtagOperator::uploadSequence(): Expected " << expected_replies
          << " replies, got " << replies.size();
      //scasw_throw_runtime_error_with_origin( oss.str() );
      LOG(Log::ERR) << oss.str();
    }

  // Extract the TDI bits and store them
  // (Note that the number of bits is rounded up to the next multiple of 32;
  //  the user needs to be aware of this, especially when he requests
  //  replies from multiple string uploads in one sequence)
  m_replyBytes.clear();
  for( Sca::Reply &r: replies )
    {
      if( r.channelId() != Sca::Constants::ChannelIds::JTAG ||
          r.error() != 0 ) // If error, exception thrown by JtagPort
        // Report or throw exception ?
        // (or let the user decide whether he got sufficient bits)
        continue;

      // Data starts at index 4 (little-endian byte mapping is: 2,3,0,1);
      // copy data bytes into the local array in the correct order
      m_replyBytes.push_back( r[4+2] );
      m_replyBytes.push_back( r[4+3] );
      m_replyBytes.push_back( r[4+0] );
      m_replyBytes.push_back( r[4+1] );
    }

  return true;
}

// ----------------------------------------------------------------------------

uint8_t const *JtagOperator::replyBytes()
{
  if( m_replyBytes.size() > 0 )
    return m_replyBytes.data();
  return 0;
}

// ----------------------------------------------------------------------------

void JtagOperator::displaySequence( bool all )
{
  m_jtagSequence.displayInfo( all );
}
  
// ----------------------------------------------------------------------------
} /* namespace HenksJtag */
