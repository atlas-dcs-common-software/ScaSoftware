/*
 * HdlcBackend.cpp
 *
 *  Created on: Oct 13, 2020
 *      Author: Paris Moschovakos (paris.moschovakos@cern.ch)
 *
 *      Description : Backend implementation for netio-next
 *
 *  1. netio-next documentation
 *  https://atlas-project-felix.web.cern.ch/atlas-project-felix/dev/staging/www/netio/topic/docs/index.html
 *
 *  2. felix-client-interface documentation
 *  https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-user-manual/versions/Latest/8_felix_star.html#_8_9_connecting_to_felix_star_using_the_felix_client_interface
 *
 */

#include <algorithm>
#include <functional>
#include <iostream>
#include <numeric>
#include <iomanip>

#include <LogIt.h>

#include <NetioNextBackend/HdlcBackend.h>
#include <NetioNextBackend/HdlcFrameTranslator.h>
#include <NetioNextBackend/HdlcBackendCommon.h>
#include <ScaCommon/ScaSwLogComponents.h>

#include "felix/felix_client_properties.h"

using Sca::LogComponentLevels;
using namespace std::chrono_literals;

namespace NetioNextBackend
{

HdlcBackend::HdlcBackend (const std::string& netioNextAddress)
                try :
                m_originalNetioNextAddress(netioNextAddress),
                m_netioNextAddress( std::make_shared<NetioNextAddress>(netioNextAddress) ),
                m_seqnr(7), // will be incremented before 1st use, and it is modulo 8,
                m_clientSubscribed(false),
                m_clientSendDataInitialized(false),
                m_felixConfigOnData( std::bind( &HdlcBackend::on_data, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4 ) ),
                m_felixConfigOnInit( std::bind( &HdlcBackend::on_init, this) ),
                m_felixConfigOnConnect( std::bind( &HdlcBackend::on_connect, this, std::placeholders::_1 ) ),
                m_felixConfigOnDisconnect( std::bind( &HdlcBackend::on_disconnect, this, std::placeholders::_1 ) ),
                m_felixConfigProperties{
                    {FELIX_CLIENT_READ_ENV, "True"}
                },
                m_felixConfig{
                    .on_data_callback = m_felixConfigOnData,
                    .on_init_callback = m_felixConfigOnInit,
                    .on_connect_callback = m_felixConfigOnConnect,
                    .on_disconnect_callback = m_felixConfigOnDisconnect,
                    .property = m_felixConfigProperties
                },
                m_felixClient(m_felixConfig)
{

    LOG(Log::INF, LogComponentLevels::netioNext()) << "Connecting to felix using felix ID from SCA: 0x" << std::hex << m_netioNextAddress->fidRx()
            << " and felix ID to SCA: 0x" << m_netioNextAddress->fidTx();
    printFelixClientProperties();

    LOG(Log::DBG, LogComponentLevels::netioNext()) << "Subscribing to felix ID: 0x" << std::hex << m_netioNextAddress->fidRx();

    // Wait until fidRx subscribed, when so continue
    std::unique_lock<decltype(m_subscribeSynchronizationMutex)> uniqueLock(m_subscribeSynchronizationMutex);
    m_felixClient.subscribe(m_netioNextAddress->fidRx());
    m_subscribedConditionVariable.wait_for(uniqueLock, 2000ms, [&](){ return m_clientSubscribed; });

    // Wait until fidTx is ready for use, when so continue
    std::unique_lock<decltype(m_initSendDataSynchronizationMutex)> uniqueLockInitSendData(m_initSendDataSynchronizationMutex);
    m_felixClient.init_send_data(m_netioNextAddress->fidTx());
    m_sendDataInitializedConditionVariable.wait_for(uniqueLockInitSendData, 2000ms, [&](){ return m_clientSendDataInitialized; });

    if (m_clientSubscribed && m_clientSendDataInitialized)
    {
        sendHdlcControl( Hdlc::HdlcControlCodes::RESET );
    }
    else if (m_clientSendDataInitialized)
    {
        std::stringstream stream;
        stream << std::hex << m_netioNextAddress->fidRx();
        scasw_throw_runtime_error_with_origin(std::string("Timeout while subscribing to fid: 0x") + stream.str() + ", felix-client subscribe() failed");
    }
    else
    {
        std::stringstream stream;
        stream << std::hex << m_netioNextAddress->fidTx();
        scasw_throw_runtime_error_with_origin(std::string("Timeout while initializing fid: 0x") + stream.str() + ", felix-client init_send_data() failed");
    }

}
catch( const std::exception &e )
{
    LOG(Log::ERR, LogComponentLevels::netioNext()) << "Caught: " << e.what() << " when initializing HdlcBackend at SCA with address: " << netioNextAddress;
    throw;
}

HdlcBackend::~HdlcBackend ()
{

    cleanUp();
    LOG(Log::TRC, LogComponentLevels::netioNext()) << "HdlcBackend dtr...";

}

void HdlcBackend::send (const Hdlc::Payload &request )
{

    LOG(Log::TRC, LogComponentLevels::netioNext()) << "Send request attempt";

    std::vector<Hdlc::Payload> requests ({ request });
    std::vector<unsigned int> times ({0});
    this->send( requests, times );

}

void HdlcBackend::send (const std::vector<Hdlc::Payload> &requests, const std::vector<unsigned int> times)
{

    if (requests.size() != times.size())
        scasw_throw_runtime_error_with_origin("requests vector has different size than times vector!");

    std::vector<const uint8_t*> netioMessagesGroup;
    std::vector<size_t> netioMessageSizes;
    std::vector<std::vector<uint8_t>> netioMessagesData;

    // comment: send() synchronization is very important to make sure that HDLC
    // sequence numbers - stored as m_seqnr - are guaranteed sequential.
    std::lock_guard<decltype(m_transmitSynchronizationMutex)> lock (m_transmitSynchronizationMutex);

    for (unsigned int i=0; i<requests.size(); ++i)
    {
        const Hdlc::Payload& request = requests[i];
        m_seqnr = (m_seqnr+1)%8;
        std::vector<uint8_t> hdlc_encoded_frame( HDLC_FRAME_PAYLOAD_OFFSET + request.size() + HDLC_FRAME_TRAILER_SIZE, 0 );
        felix::hdlc::encode_hdlc_msg_header( &hdlc_encoded_frame[0], /*HDLC address, always 0 for SCA*/0, m_seqnr);
        std::copy( request.cbegin(), request.cend(), &hdlc_encoded_frame[ HDLC_FRAME_PAYLOAD_OFFSET] );
        felix::hdlc::encode_hdlc_trailer(
                &hdlc_encoded_frame[ HDLC_FRAME_PAYLOAD_OFFSET + request.size()], /* where to put the trailer */
                &hdlc_encoded_frame[0], /* pointer to the beginning of HDLC frame*/
                request.size() + HDLC_FRAME_PAYLOAD_OFFSET);

        LOG(Log::TRC, LogComponentLevels::netioNext()) << "Pushing back data frame:" << vectorAsHex(hdlc_encoded_frame);
        netioMessagesData.push_back(hdlc_encoded_frame);
        netioMessageSizes.push_back(hdlc_encoded_frame.size());

        if (times[i] > 0)
        {
            /* We will glue a block of zeros after the contents for a delay in Henk B. method */
            std::vector<unsigned char> numBytes(FLX_ZEROS_PER_MICROSECOND * times[i], 0);
            LOG(Log::TRC, LogComponentLevels::netioNext()) << "Pushing back time frame:" << vectorAsHex(numBytes);
            netioMessagesData.push_back(numBytes);
            netioMessageSizes.push_back(numBytes.size());
        }

        m_statistician.onFrameSent();
    }

    int netioNextFrameSize = std::accumulate(netioMessageSizes.begin(), netioMessageSizes.end(), 0);
    LOG(Log::DBG, LogComponentLevels::netioNext()) << "sending netio-next request frame, group_size=" << requests.size() << ", frame_size=" << netioNextFrameSize;
    LOG(Log::DBG, LogComponentLevels::netioNext()) << "contents: " << vectorOfVectorAsHex(netioMessagesData);

    for ( std::vector<uint8_t>& messages : netioMessagesData )
    {
        netioMessagesGroup.push_back(&messages[0]);
    }

    try
    {
        m_felixClient.send_data_nb(m_netioNextAddress->fidTx(), netioMessagesGroup, netioMessageSizes);
    }
    catch(const std::exception& e)
    {
        LOG(Log::ERR, LogComponentLevels::netioNext()) <<
                 "While sending at address=" << m_netioNextAddress->toString() <<
                 " Caught: " << e.what();
    }

}

void HdlcBackend::sendHdlcControl (uint8_t hdlcControl)
{
    // Both CONNECT and RESET HDLC OPs reset the sequence of HDLC

    std::vector<unsigned char> netioNextFrame ( HDLC_FRAME_PAYLOAD_OFFSET + HDLC_FRAME_TRAILER_SIZE );

    felix::hdlc::encode_hdlc_ctrl_header( &netioNextFrame[0], 0, hdlcControl );
    felix::hdlc::encode_hdlc_trailer(
            &netioNextFrame[0] + HDLC_FRAME_PAYLOAD_OFFSET, /* where to put the trailer */
            &netioNextFrame[0],
            HDLC_FRAME_PAYLOAD_OFFSET);

    LOG(Log::DBG, LogComponentLevels::netioNext()) << "Sending control: " << std::hex << (unsigned int)hdlcControl;
    LOG(Log::TRC, LogComponentLevels::netioNext()) <<
            "Size: " << netioNextFrame.size() <<
            " contents: " << vectorAsHex(netioNextFrame);

    try
    {
        m_felixClient.send_data_nb(m_netioNextAddress->fidTx(), &netioNextFrame[0], netioNextFrame.size(), true);
    }
    catch(const std::exception& e)
    {
        LOG(Log::ERR, LogComponentLevels::netioNext()) <<
                 "While sending HDLC control at address=" << m_netioNextAddress->toString() <<
                 " Caught: " << e.what();
    }

}

void HdlcBackend::unSubscribeReceiver()
{

    m_receiver = nullptr;
    cleanUp();

}

void HdlcBackend::cleanUp ()
{
    LOG(Log::INF, LogComponentLevels::netioNext()) << "Cleaning up felix ID 0x" << std::hex << m_netioNextAddress->fidRx();

    if (m_clientSubscribed)
    {
        try
        {
            m_felixClient.unsubscribe(m_netioNextAddress->fidRx());
        }
        catch (const std::exception& e)
        {
            LOG(Log::ERR, LogComponentLevels::netioNext()) <<
                    "While unsubscribing at address=" << m_netioNextAddress->toString() <<
                    " Caught: " << e.what();
        }
        m_clientSubscribed = false;
    }

    LOG(Log::TRC, LogComponentLevels::netioNext()) <<
            "At address: " << m_netioNextAddress->toString() <<
            " cleanUp() of subscribe socket finished";
}

void HdlcBackend::on_init()
{

    LOG(Log::DBG, LogComponentLevels::netioNext()) << "Felix client initializing...";

}

void HdlcBackend::on_connect(uint64_t fid)
{

    LOG(Log::DBG, LogComponentLevels::netioNext()) << "Connection established for felix ID 0x" << std::hex << fid;

    if ( fid == m_netioNextAddress->fidRx() )
    {
        std::lock_guard<decltype(m_subscribeSynchronizationMutex)> lockGuard(m_subscribeSynchronizationMutex);

        m_clientSubscribed = true;
        m_subscribedConditionVariable.notify_one();
    }
    else if ( fid == m_netioNextAddress->fidTx() )
    {
        std::lock_guard<decltype(m_initSendDataSynchronizationMutex)> lockGuard(m_initSendDataSynchronizationMutex);

        m_clientSendDataInitialized = true;
        m_sendDataInitializedConditionVariable.notify_one();
    }
    else
    {
        LOG(Log::ERR, LogComponentLevels::netioNext()) << "Felix Client fired on_connect() for an unknown felix ID 0x" << std::hex << fid;
    }

}

void HdlcBackend::on_disconnect(uint64_t fid)
{

    LOG(Log::DBG, LogComponentLevels::netioNext()) << "felix ID 0x" << std::hex << fid << " was disconnected";

    if ( fid == m_netioNextAddress->fidRx() )
    {
        std::lock_guard<decltype(m_subscribeSynchronizationMutex)> lockGuard(m_subscribeSynchronizationMutex);

        m_clientSubscribed = false;
    }
    else if ( fid == m_netioNextAddress->fidTx() )
    {
        std::lock_guard<decltype(m_initSendDataSynchronizationMutex)> lockGuard(m_initSendDataSynchronizationMutex);

        m_clientSendDataInitialized = false;
    }
    else
    {
        LOG(Log::ERR, LogComponentLevels::netioNext()) << "Felix Client fired on_disconnect() for an unknown felix ID 0x" << std::hex << fid;
    }

}

void HdlcBackend::on_data(uint64_t fid, const uint8_t* data, size_t size, uint8_t status)
{

    LOG(Log::TRC, LogComponentLevels::netioNext()) << "At SCA with address: '" + getAddress() + "' received reply";

    if ( size > 20 )
    {
        LOG(Log::ERR, LogComponentLevels::netioNext()) << "At SCA with address: '" + getAddress() + "' avoiding possibly extra large netIO frame. (size=" << (int)size << " bytes)";
        return;
    }

    std::vector<uint8_t> netioNextFrame (data, data + size);

    // Valid SCA frame sizes are 8, 10 and 12 bytes.
    // Valid SCA HDLC frames can also be 4 (UA) and 6 (SREJ) bytes.
    switch(netioNextFrame.size()) {
            case 8 :
            case 10 :
            case 12 :
                    break;

            case 4 :
            case 6 : {
                    HdlcFrameTranslator HdlcFrameTranslator(netioNextFrame, m_netioNextAddress);
                    return;
            }

            default: {
                     LOG(Log::ERR, LogComponentLevels::netioNext()) << "At SCA with address: '" + getAddress() + "' "
                         << "discarding incoming netio-next frame containing an SCA reply of unexpected size. SCA reply size was "
                         << netioNextFrame.size() << " bytes (expected SCA replies sizes are 4, 6, 8, 10 or 12 bytes)";
                     LOG(Log::DBG, LogComponentLevels::netioNext()) << "At SCA with address: '" + getAddress() + "' raw bytes of the netio-next frame with unexpected size: 0x " << vectorAsHex(netioNextFrame);
                     return;
             }
         }

    // Validate HDLC FCS
    if (!validateFcs(netioNextFrame.begin(), netioNextFrame.end()))
    {
        LOG(Log::ERR, LogComponentLevels::netioNext()) << "At SCA with address: '" + getAddress() + "' HDLC checksum mismatch on incoming frame, discarding";
        LOG(Log::DBG, LogComponentLevels::netioNext()) << "At SCA with address: '" + getAddress() + "' raw bytes of the netIO frame with invalid FCS: 0x " << vectorAsHex(netioNextFrame);
        return;
    }

    // Cut the Address and the Control field from the HDLC frame | and the FCS at the end -- Paris
    uint8_t* hdlcPayloadPtr = &netioNextFrame[ HDLC_FRAME_PAYLOAD_OFFSET ];

    try // the following may throw if the reply is mutilated, too big, coming after unsubscribe etc.
    {
        Hdlc::Payload payload( hdlcPayloadPtr, netioNextFrame.size() - HDLC_FRAME_PAYLOAD_OFFSET - HDLC_FRAME_TRAILER_SIZE);
        m_receiver(payload);
    }
    catch (const std::bad_function_call& e)
    {
        LOG(Log::ERR, LogComponentLevels::netioNext()) << "Ignoring reply for unsubscribed SCA with address: '" + getAddress() + "'";
    }
    catch (const std::exception& e)
    {
        LOG(Log::TRC, LogComponentLevels::netioNext()) << "At SCA with address: '" + getAddress() + "' ignoring reply: " << e.what();
    }

    m_statistician.onFrameReceived();

}

void HdlcBackend::printFelixClientProperties()
{

    LOG(Log::INF, LogComponentLevels::netioNext()) << "List of (known) felix-client-properties used: " << "\n"
            << std::setw(86) << FELIX_CLIENT_LOCAL_IP_OR_INTERFACE << ": " << m_felixConfig.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] << "\n"
            << std::setw(86) << FELIX_CLIENT_LOG_LEVEL << ": "             << m_felixConfig.property[FELIX_CLIENT_LOG_LEVEL] << "\n"
            << std::setw(86) << FELIX_CLIENT_BUS_DIR << ": "               << m_felixConfig.property[FELIX_CLIENT_BUS_DIR] << "\n"
            << std::setw(86) << FELIX_CLIENT_BUS_GROUP_NAME << ": "        << m_felixConfig.property[FELIX_CLIENT_BUS_GROUP_NAME] << "\n"
            << std::setw(86) << FELIX_CLIENT_VERBOSE_BUS << ": "           << m_felixConfig.property[FELIX_CLIENT_VERBOSE_BUS] << "\n"
            << std::setw(86) << FELIX_CLIENT_TIMEOUT << ": "               << m_felixConfig.property[FELIX_CLIENT_TIMEOUT] << "\n"
            << std::setw(86) << FELIX_CLIENT_NETIO_PAGES << ": "           << m_felixConfig.property[FELIX_CLIENT_NETIO_PAGES] << "\n"
            << std::setw(86) << FELIX_CLIENT_NETIO_PAGESIZE << ": "        << m_felixConfig.property[FELIX_CLIENT_NETIO_PAGESIZE] << "\n"
            << std::setw(86) << FELIX_CLIENT_THREAD_AFFINITY << ": "       << m_felixConfig.property[FELIX_CLIENT_THREAD_AFFINITY];

}

}
