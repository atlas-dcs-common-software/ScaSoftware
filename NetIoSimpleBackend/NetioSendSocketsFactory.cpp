/*
 * NetioSendSocketsFactory.cpp
 *
 *  Created on: 25 Nov 2019
 *      Author: pnikiel
 */

#include <algorithm>

#include <LogIt.h>
#include <NetIoSimpleBackend/NetioSendSocketsFactory.h>
#include <ScaCommon/ScaSwLogComponents.h>

using Sca::LogComponentLevels;

namespace NetIoSimpleBackend
{

netio::low_latency_send_socket& NetioSendSocketsFactory::requestSendSocket (
        const HdlcBackend* requestorId,
        const std::string& felixcoreHostName,
        unsigned int portToSca)
{
    std::lock_guard<decltype(s_accessLock)> lock (s_accessLock);

    ensureEventLoopStarted();

    ConnectionId requestedConnectionId (felixcoreHostName, portToSca);
    auto iter = std::find_if(
            s_factoryState.begin(),
            s_factoryState.end(),
            [&requestedConnectionId](const SendSocketContainer &p){return p.connectionId == requestedConnectionId;});
    if (iter == s_factoryState.end()) // this is requested the first time really
    {
        LOG(Log::DBG, LogComponentLevels::netio()) << "Creating new send_socket for: " << requestedConnectionId.toString();
        s_factoryState.emplace_back(requestedConnectionId);
        try
        {
            s_factoryState.back().socket.connect(
                    netio::endpoint(
                                    felixcoreHostName,
                                    portToSca));
        }
        catch (const std::exception& e)
        {
            // the last object is misconstructed ( send_socket is there but it is not connected - remove it from the factory )
            LOG(Log::ERR, LogComponentLevels::netio()) << "netio::send_socket::connect() failed for: " << s_factoryState.back().connectionId.toString() << " with " << e.what();
            s_factoryState.pop_back();
            if (s_factoryState.empty())
                ensureEventLoopStopped();
            throw;
        }
        s_factoryState.back().users.push_back(requestorId);
        return s_factoryState.back().socket;
    }
    else
    {
        // add to the users list only if not there already
        if (std::none_of(
                iter->users.begin(),
                iter->users.end(),
                [requestorId](const HdlcBackend* user){ return user == requestorId;}))
            iter->users.push_back(requestorId);
        LOG(Log::DBG, LogComponentLevels::netio()) << "Reusing request socket, for: " << requestedConnectionId.toString()
                << ", new usage count=" << iter->users.size();
        return iter->socket;
    }
}

void NetioSendSocketsFactory::releaseSendSocket ( const HdlcBackend* requestorId )
{
    std::lock_guard<decltype(s_accessLock)> lock (s_accessLock);

    // need iterator instead of range-for because of removal
    for (auto it = s_factoryState.begin(); it != s_factoryState.end(); it++)
    {
        SendSocketContainer& sendSocketContainer = *it;
        sendSocketContainer.users.remove(requestorId);
        if (sendSocketContainer.users.empty())
        {
            LOG(Log::DBG, LogComponentLevels::netio()) << "Looks like this felixcore connection: " << sendSocketContainer.connectionId.toString() << " is no longer used, disposing.";
            try
            {
                sendSocketContainer.socket.disconnect();
            }
            catch (const std::exception &e)
            {
                LOG(Log::ERR, LogComponentLevels::netio()) << "While disconnecting: " << e.what();
            }
	    s_factoryState.erase(it);
            break;
        }
    }

    // and now the part to see if the event loop should be stopped -- if the factory has at least one connection, it shoudl not be
    if (s_factoryState.empty())
    {
        ensureEventLoopStopped();
        LOG(Log::DBG, LogComponentLevels::netio()) << "Stopping event loop";
    }
}

void NetioSendSocketsFactory::ensureEventLoopStarted()
{

    if (!s_netioContextEventLoopThread.joinable())
    {
        s_netioContextEventLoopThread = std::thread( []() {
            try
            {
                s_netioContext.event_loop()->run_forever();
            }
            catch (const std::exception &e)
            {
                LOG(Log::ERR, LogComponentLevels::netio()) << "FATAL: Event loop thrown: " << e.what();
            }
            } );
	LOG(Log::INF, LogComponentLevels::netio()) << "Event loop thread does not exists. Created one with id: " << s_netioContextEventLoopThread.get_id();
    }
    LOG(Log::DBG, LogComponentLevels::netio()) << "Event loop thread already exists id: " << s_netioContextEventLoopThread.get_id();

}

void NetioSendSocketsFactory::ensureEventLoopStopped()
{
    if (s_netioContextEventLoopThread.joinable())
    {
        try
        {
            s_netioContext.event_loop()->stop();
        }
        catch (const std::exception &e)
        {
            LOG(Log::ERR, LogComponentLevels::netio()) << "Stopping the event loop thrown: " << e.what();
        }
        LOG(Log::DBG, LogComponentLevels::netio()) << "Event loop stopped";
	
	LOG(Log::INF, LogComponentLevels::netio()) << "Joining thread with id: " << s_netioContextEventLoopThread.get_id();
	s_netioContextEventLoopThread.join();
    }
}

std::list<NetioSendSocketsFactory::SendSocketContainer> NetioSendSocketsFactory::s_factoryState;
std::thread NetioSendSocketsFactory::s_netioContextEventLoopThread;
netio::context NetioSendSocketsFactory::s_netioContext ("posix");
std::mutex NetioSendSocketsFactory::s_accessLock;

}
