#include <LogIt.h>
#include <algorithm>

#include <NetIoSimpleBackend/HdlcBackend.h>
#include <NetIoSimpleBackend/SimpleNetioSpecificAddress.h>
#include <iostream>

#include <felixbase/client.hpp>
#include <NetIoSimpleBackend/HdlcFrameTranslator.h>
#include <ScaCommon/ScaSwLogComponents.h>
#include <NetIoSimpleBackend/HdlcBackendCommon.h>
#include <NetIoSimpleBackend/NetioSendSocketsFactory.h>

// http://atlas-felix.cern.ch/netio/

using Sca::LogComponentLevels;

namespace NetIoSimpleBackend
{

HdlcBackend::HdlcBackend (const std::string& specificAddress):
        		m_originalSpecificAddress(specificAddress),
				m_specificAddress( std::make_shared<SpecificAddress>(specificAddress) ),
				m_netioContext("posix"),
				m_netioRequestSocket(
				        NetioSendSocketsFactory::requestSendSocket(
				                this,
				                m_specificAddress->felixHostname(),
				                m_specificAddress->portToSca()) ),
				m_netioReplySocket( &m_netioContext, std::bind(&HdlcBackend::replyHandler, this, std::placeholders::_1, std::placeholders::_2) ),
				m_seqnr (7), // will be incremented before 1st use, and it is modulo 8,
                m_replySocketSubscribed(false)
{

	LOG(Log::INF, LogComponentLevels::netio()) << "Connecting to " << m_specificAddress->felixHostname()
			<< ", port to SCA: " << m_specificAddress->portToSca()
			<< ", SCA elink: TX: 0x" << std::hex << m_specificAddress->elinkIdTx() << " RX: 0x" << std::hex << m_specificAddress->elinkIdRx();
	m_netioThread = boost::thread( [this](){ netioMainThread(); } );

	try
	{
	    LOG(Log::DBG, LogComponentLevels::netio()) << "Subscribing to elink: " << std::hex << (int)m_specificAddress->elinkIdRx();
	    m_netioReplySocket.subscribe( m_specificAddress->elinkIdRx(), netio::endpoint( m_specificAddress->felixHostname(), m_specificAddress->portFromSca() ));
	    m_replySocketSubscribed = true;
	    usleep(20000);
	    sendHdlcControl( Hdlc::HdlcControlCodes::RESET );
	}
	catch (const std::exception &e )
	{
	    LOG(Log::ERR, LogComponentLevels::netio()) << "Exception: " << e.what();
	    cleanUp();
	    throw e;
	}
}

HdlcBackend::~HdlcBackend ()
{
    LOG(Log::DBG, LogComponentLevels::netio()) << "Cleaning up from HdlcBackend dtr...";
    cleanUp ();

    // We want to release the send socket only in the destructor, so that the coherency of the object remains assured (the socket is kept as reference)
    NetioSendSocketsFactory::releaseSendSocket(this);
}


void HdlcBackend::send (const Hdlc::Payload &request )
{
    std::vector<Hdlc::Payload> requests ({ request });
    std::vector<unsigned int> times ({0});
    this->send( requests, times );
}

void HdlcBackend::send (
          const std::vector<Hdlc::Payload> &requests,
          const std::vector<unsigned int> times
      )
{

    if (requests.size() != times.size())
        scasw_throw_runtime_error_with_origin("requests vector has different size than times vector!");

    std::vector<unsigned char> netioFrame;
    netioFrame.reserve( 8192 );

    // comment: send() synchronization is very important to make sure that HDLC
    // sequence numbers - stored as m_seqnr - are guaranteed sequential.
    std::lock_guard<decltype(m_transmitSynchronizationMutex)> lock (m_transmitSynchronizationMutex);

    for (unsigned int i=0; i<requests.size(); ++i)
    {
        const Hdlc::Payload& request = requests[i];
        m_seqnr = (m_seqnr+1)%8;
        std::vector<uint8_t> hdlc_encoded_frame( HDLC_FRAME_PAYLOAD_OFFSET + request.size() + HDLC_FRAME_TRAILER_SIZE, 0 );
        felix::hdlc::encode_hdlc_msg_header( &hdlc_encoded_frame[0], /*HDLC address, always 0 for SCA*/0, m_seqnr);
        std::copy( request.cbegin(), request.cend(), &hdlc_encoded_frame[ HDLC_FRAME_PAYLOAD_OFFSET] );
        felix::hdlc::encode_hdlc_trailer(
                &hdlc_encoded_frame[ HDLC_FRAME_PAYLOAD_OFFSET + request.size()], /* where to put the trailer */
                &hdlc_encoded_frame[0], /* pointer to the beginning of HDLC frame*/
                request.size() + HDLC_FRAME_PAYLOAD_OFFSET);


        felix::base::ToFELIXHeader header;
        header.length = hdlc_encoded_frame.size();
        header.reserved = 0;
        header.elinkid = m_specificAddress->elinkIdTx();
        uint8_t * const headerPtr = reinterpret_cast<uint8_t*>(&header);

        std::vector<uint8_t> thisChunk ( hdlc_encoded_frame.size() + sizeof(header), 0 );
        std::copy(
                headerPtr,
                headerPtr + sizeof(header),
                &thisChunk[0] );

        std::copy(
                hdlc_encoded_frame.begin(),
                hdlc_encoded_frame.end(),
                &thisChunk[sizeof header] );

        netioFrame.insert( netioFrame.end(), thisChunk.begin(), thisChunk.end() );

        if (times[i] > 0)
        {
            /* We will glue a block of zeros after the contents for a delay in Henk B. method */
            const unsigned int numBytes = FLX_ZEROS_PER_MICROSECOND * times[i];
            header.length = numBytes;
            netioFrame.insert( netioFrame.end(), headerPtr, headerPtr+sizeof header );
            netioFrame.insert( netioFrame.end(), numBytes, 0);
        }

        m_statistician.onFrameSent();
    }

    LOG(Log::TRC, LogComponentLevels::netio()) << "sending netio request frame, group_size=" << requests.size() << ", frame_size=" << netioFrame.size();

    LOG(Log::TRC, LogComponentLevels::netio()) << "contents: " << vectorAsHex(netioFrame);


    netio::message msg( &netioFrame[0], netioFrame.size() );

    m_netioRequestSocket.send( msg );

}

void HdlcBackend::netioMainThread(void)
{
	LOG(Log::DBG, LogComponentLevels::netio()) << "Inside netio thread for address: " << m_originalSpecificAddress;
    try
    {
        m_netioContext.event_loop()->run_forever();
    }
    catch (const std::exception& e)
    {
        LOG(Log::ERR, LogComponentLevels::netio()) << "From netio run_forever(): " << e.what();
    }
}

void HdlcBackend::replyHandler( netio::endpoint&, netio::message& m )
{
    LOG(Log::TRC, LogComponentLevels::netio()) << "At SCA with address: '" + getAddress() + "' received reply";

    if ( m.size() > 20 )
    {
        LOG(Log::ERR, LogComponentLevels::netio()) << "At SCA with address: '" + getAddress() + "' avoiding possibly extra large netIO frame. (size=" << m.size() << " bytes)";
        return;
    }

    try // the following may throw if the reply is mutilated, too big, etc.
    {
        std::vector<uint8_t> netioFrame( m.size() );
        m.serialize_to_usr_buffer( netioFrame.data() );

        // Valid SCA frame sizes are 8, 10 and 12 bytes.
        // Valid SCA HDLC frames can also be 4 (UA) and 6 (SREJ) bytes.
        switch(netioFrame.size() - sizeof(felix::base::FromFELIXHeader)) {
            case 8 :
            case 10 :
            case 12 :
                    break;

            case 4 :
            case 6 : {
                    std::vector<uint8_t> hdlcFrame(netioFrame.begin() + sizeof(felix::base::FromFELIXHeader), netioFrame.end());
                    HdlcFrameTranslator HdlcFrameTranslator(hdlcFrame, m_specificAddress);
                    return;
            }

            default: {
                    LOG(Log::ERR, LogComponentLevels::netio()) << "At SCA with address: '" + getAddress() + "' "
                        << "discarding incoming netIO frame containing an SCA reply of unexpected size. SCA reply size was "
                        << netioFrame.size() - sizeof(felix::base::FromFELIXHeader) << " bytes (expected SCA replies sizes are 4, 6, 8, 10 or 12 bytes)";
                    LOG(Log::DBG, LogComponentLevels::netio()) << "At SCA with address: '" + getAddress() + "' raw bytes of the netIO frame with unexpected size: 0x " << vectorAsHex(netioFrame);
                    return;
            }
        }

        // Validate HDLC FCS
        if (!validateFcs(netioFrame.begin() + sizeof(felix::base::FromFELIXHeader), netioFrame.end()))
        {
            LOG(Log::ERR, LogComponentLevels::netio()) << "At SCA with address: '" + getAddress() + "' HDLC checksum mismatch on incoming frame, discarding";
            LOG(Log::DBG, LogComponentLevels::netio()) << "At SCA with address: '" + getAddress() + "' raw bytes of the netIO frame with invalid FCS: 0x " << vectorAsHex(netioFrame);
            return;
        }

        // Cut the Address and the Control field from the HDLC frame | and the FCS at the end -- Paris
        uint8_t* hdlcPayloadPtr = &netioFrame[ NETIO_FRAME_HDLC_START_OFFSET + HDLC_FRAME_PAYLOAD_OFFSET ];


        Hdlc::Payload payload( hdlcPayloadPtr, netioFrame.size() - NETIO_FRAME_HDLC_START_OFFSET - HDLC_FRAME_PAYLOAD_OFFSET - HDLC_FRAME_TRAILER_SIZE);
        m_receiver(payload);
    }
    catch (const std::exception& e)
    {
        LOG(Log::TRC, LogComponentLevels::netio()) << "At SCA with address: '" + getAddress() + "' ignoring reply because: " << e.what();
    }
    m_statistician.onFrameReceived();
}

void HdlcBackend::sendHdlcControl (uint8_t hdlcControl)
{
    // Both CONNECT and RESET HDLC OPs reset the sequence of HDLC
    felix::base::ToFELIXHeader header;

    std::vector<unsigned char> netioFrame ( sizeof(header) + HDLC_FRAME_PAYLOAD_OFFSET + HDLC_FRAME_TRAILER_SIZE );

    header.length = netioFrame.size() - sizeof(header);
    header.reserved = 0;
    header.elinkid = m_specificAddress->elinkIdTx();

    std::copy(
            reinterpret_cast<uint8_t*>(&header),
            reinterpret_cast<uint8_t*>(&header)+sizeof header,
            netioFrame.begin() );
	felix::hdlc::encode_hdlc_ctrl_header( &netioFrame[ sizeof header ], 0, hdlcControl );
	felix::hdlc::encode_hdlc_trailer(
	        &netioFrame[0] + sizeof(header) + HDLC_FRAME_PAYLOAD_OFFSET, /* where to put the trailer */
	        &netioFrame[0] + sizeof(header),
	        HDLC_FRAME_PAYLOAD_OFFSET);
	netio::message message( &netioFrame[0], netioFrame.size() );
	LOG(Log::DBG, LogComponentLevels::netio()) << "Sending control: " << std::hex << (unsigned int)hdlcControl;
	LOG(Log::TRC, LogComponentLevels::netio()) << "Size: " << netioFrame.size() << " contents: " << vectorAsHex(netioFrame);
	m_netioRequestSocket.send(message);
}

void HdlcBackend::cleanUp ()
{
    LOG(Log::INF, LogComponentLevels::netio()) << "Disconnecting from " << m_originalSpecificAddress;

    if (m_replySocketSubscribed)
    {
        try
        {
            m_netioReplySocket.unsubscribe( m_specificAddress->elinkIdRx(), netio::endpoint( m_specificAddress->felixHostname(), m_specificAddress->portFromSca() ));
        }
        catch (const std::exception& e)
        {
            LOG(Log::ERR, LogComponentLevels::netio()) <<
                    "While unsubscribing at address=" << m_originalSpecificAddress <<
                    "Caught: " << e.what();
        }
        m_replySocketSubscribed = false;
    }
    try
    {
        m_netioContext.event_loop()->stop();
    }
    catch (const std::exception& e)
    {
        LOG(Log::ERR, LogComponentLevels::netio()) <<
                "While stopping event_loop for address=" << m_originalSpecificAddress <<
                "Caught: " << e.what();
    }
    LOG(Log::DBG, LogComponentLevels::netio()) << "Waiting to join the netio thread";
    m_netioThread.join();

    LOG(Log::TRC, LogComponentLevels::netio()) <<
            "At address: " << m_originalSpecificAddress <<
            "cleanUp() of subscribe socket finished";
}

}
