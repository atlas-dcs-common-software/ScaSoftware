#!/usr/bin/env bash

echo "Package tools: trying to create a package for commit id $1"
DIR=ScaSoftwareTools-$1
rm -Rf $DIR $DIR".tar.gz"

mkdir -p $DIR/bin
mkdir -p $DIR/lib

cp -v build/Demonstrators/Timing/timing \
        build/Demonstrators/SimulatorOverNetIo/simulator_netio \
        build/Demonstrators/StandaloneI2cSimulation/standalone_i2c \
        build/Demonstrators/GbtxConfiguration/gbtx_configuration \
        build/Demonstrators/HenksFScaJtag/fscajtag \
        build/Demonstrators/HenksFScaJtag/fscajfile \
        build/Demonstrators/HenksFScaJtag/fxvcserver \
        build/Demonstrators/PingSca/ping_sca \
        build/Demonstrators/StandaloneAdcSimulation/standalone_adc \
        build/Demonstrators/StandaloneDacSimulation/standalone_dac \
        build/Demonstrators/StandaloneSpiSimulation/standalone_spi \
        Demonstrators/README.md \
        $DIR/bin || exit 1

cp -vL ${FELIX_ROOT}/lib/libfelix-client-thread.so \
    ${FELIX_ROOT}/lib/libnetio.so \
    ${FELIX_ROOT}/lib/libhdlc_coder.so \
    ${FELIX_ROOT}/lib/libtbb.so.2 \
    ${FELIX_ROOT}/lib/libfabric.so.1 \
    ${FELIX_ROOT}/lib/libatomic.so.1 \
    ${FELIX_ROOT}/lib/libfelix-client-lib.so \
    ${FELIX_ROOT}/lib/libnetio-lib.so \
    ${FELIX_ROOT}/lib/libfelix-bus-lib.so \
    ${FELIX_ROOT}/lib/libfelix-bus-server-lib.so \
    ${FELIX_ROOT}/lib/libjwrite.so \
    ${FELIX_ROOT}/lib/libsimdjson.so \
    $DIR/lib || exit 1

cp -vL $PROTOBUF_DIRECTORIES/libprotobuf.so.* \
    $BOOST_ROOT/lib/libboost_atomic*.so.* \
    $BOOST_ROOT/lib/libboost_date_time*.so.* \
    $BOOST_ROOT/lib/libboost_chrono*.so.* \
    $BOOST_ROOT/lib/libboost_filesystem*.so.* \
    $BOOST_ROOT/lib/libboost_program_options*.so.* \
    $BOOST_ROOT/lib/libboost_regex*.so.* \
    $BOOST_ROOT/lib/libboost_system*.so.* \
    $BOOST_ROOT/lib/libboost_thread*.so.* \
    "`dirname $CXX`/../lib64/libstdc++.so.6" \
    "`dirname $CXX`/../lib64/libgcc_s.so.1" \
    $DIR/lib/ || exit 1

tar cf $DIR".tar" $DIR
gzip -9 $DIR".tar"
