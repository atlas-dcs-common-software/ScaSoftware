Standalone Demonstrators
------------------------

The standalone demonstrators do not rely on the OPC UA stack and for that are lightweight C++ applications to communicate with SCAs over netIO.
They can run on a felix or a non-felix machine and communicate over the network with a running felixcore.

Why am I failing to run a demonstrator?

The basic dependencies that the SCA standalone demonstrators require in order to run are:
1. Boost libraries: Having cvmfs available is enough. There is nothing else to do.
2. Felix libraries: This can be done by sourcing the setup.sh script provided with every FELIX release. 

Note: Always use the demonstrators with their corresponding FELIX release to avoid incompatibilities. The correspondence is currently mentioned in every release of the SCA OPC UA server here: https://gitlab.cern.ch/atlas-dcs-opcua-servers/ScaOpcUa/-/releases. The tags of SCA OPC UA match the ones of ScaSoftware. 
