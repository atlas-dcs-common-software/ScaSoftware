/*
 * main.cpp
 *
 *  Created on: Jul 17, 2020
 *      Author: pnikiel, pmoschov
 */

#include <Hdlc/BackendFactory.h>
#include <Sca/Defs.h>
#include <Sca/SynchronousService.h>
#include <Sca/Sca.h>
#include <LogIt.h>
#include <LogLevels.h>
#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <stdlib.h>
#include <stdexcept>
#include <boost/program_options.hpp>
#include <DemonstratorCommons.h>
#include <thread>
#include <ctime>

const std::string StyleRed ("\033[1;31m");
const std::string StyleBlue ("\033[1;32m");
const std::string StyleReset ("\033[0m");

#ifdef WITH_EVENT_RECORDER
#include <VeryHipEvents/EventRecorder.h>
using VeryHipEvents::Recorder;
#endif

double subtractTimeval (const timeval &t1, const timeval &t2)
{

    return t2.tv_sec-t1.tv_sec + double(t2.tv_usec-t1.tv_usec)/1000000.0;

}

struct CommonInfo
{
    std::string address;
    double      rate;
    bool        isAlive;
};

struct Config
{
    std::vector<std::string> addresses;
    double                   frequency;
    unsigned int             iterations;
};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    using namespace ScaSwDemonstrators;
    Config config;
    options_description options;
    std::string logLevelStr;
    options.add_options()
	        ("help,h", "show help")
	        ("trace_level,t", value<std::string>(&logLevelStr)->default_value("INF"),                   "Trace level, one of: ERR,WRN,INF,DBG,TRC")
	        ("addresses",     value<std::vector<std::string>>(&config.addresses),                       "list of SCA addresses to talk to")
	        ("iterations,i",  value<unsigned int>(&config.iterations)->default_value(10),               "How many connection open/close to perform per every SCA")
	        ;
    positional_options_description p;
    p.add("addresses", -1);

    variables_map vm;
    store( command_line_parser(argc,argv).options(options).positional(p).run(), vm );
    notify (vm);
    if (vm.count("help"))
    {
        std::cout << options << '\n';
        exit(1);
    }
    if (vm.count("addresses") < 1)
    {
        LOG(Log::ERR) << "No SCA addresses were specified. Doesn't make sense to run with 0 SCAs. Quitting.";
        exit(1);
    }
    initializeLogging(logLevelStr);
    return config;
}

void keepRunningForTime (CommonInfo* info, unsigned int numSeconds)
{
    Sca::Sca sca ( info->address );
    uint32_t id = sca.readChipId();
    info->isAlive = true;
    info->rate = 0;
    LOG(Log::INF) << "Note: SCA " << info->address << " initialized, id=" << id;
    timeval last_time, new_time;
    double timediff;
    gettimeofday(&last_time, 0);
    do
    {
        info->isAlive = true;
        sca.readChipId(); // 1 SCA transaction.
        gettimeofday(&new_time, 0);
        timediff = subtractTimeval(last_time, new_time);
    }
        while (timediff < numSeconds);
    LOG(Log::INF) << "Note: SCA " << info->address << " will be disposed of.";
}

void runner (CommonInfo* info, unsigned int secondsRunning, unsigned int secondsPaused, unsigned int iterations)
{
    LOG(Log::INF) << "In the thread for " << info->address;
    for (unsigned int i=0; i<iterations; i++)
    {
        try
        {
            keepRunningForTime(info, secondsRunning);
            usleep (secondsPaused * 1E6);
        }
        catch (const std::exception& e)
        {
            LOG(Log::ERR) << StyleRed << "In the thread of SCA " << info->address << " exception: " << e.what();
            info->isAlive = false;
        }
    }
    LOG(Log::INF) << StyleBlue << "In the thread for " << info->address << " all iterations completed." << StyleReset;
}

int main (int argc, char* argv[])
{
    Config config = parseProgramOptions( argc, argv);
    srand(time(0));

    #ifdef WITH_EVENT_RECORDER
    Recorder::initialize (1E6, 1E8 );
    #endif // WITH_EVENT_RECORDER

    LOG(Log::INF) << "Will try to run with " << config.addresses.size() << " SCAs";

    std::list<std::jthread> threads;
    std::list<CommonInfo> commonInfo;

    for (auto addr : config.addresses)
    {
        CommonInfo info;
        info.address = addr;
        commonInfo.push_back(info);
        threads.emplace_back(runner, &commonInfo.back(), 5, 5, config.iterations);
    }

    unsigned int iterationsCompleted = 0;
    while (iterationsCompleted < config.addresses.size() * config.iterations)
    {
        usleep (4000000);
        size_t howManyAlive = 0;
        for (auto& info : commonInfo)
        {
            if (info.isAlive)
            {
                howManyAlive++;
            }

        }
        LOG(Log::INF) << StyleBlue << "Note: " << howManyAlive << " SCAs are still alive" << StyleReset;
        iterationsCompleted += howManyAlive;
    }

    for (auto& thread : threads)
    {
        thread.join();
    }

    #ifdef WITH_EVENT_RECORDER
    Recorder::dump("dump.xml");
    #endif // WITH_EVENT_RECORDER
}