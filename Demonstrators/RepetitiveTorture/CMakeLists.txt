link_directories( ${DEMONSTRATORS_COMMON_LIBDIRS} )

set(CMAKE_BUILD_WITH_INSTALL_RPATH TRUE)
set(CMAKE_INSTALL_RPATH "$ORIGIN/../../lib")

add_executable(repetitive_torture 
  main.cpp
  ${DEMONSTRATORS_COMMON_OBJS}
  )

target_link_libraries(repetitive_torture ${DEMONSTRATORS_COMMON_LIBS} )