/*
 * main.cpp
 *
 *  Created on: May 3, 2016
 *      Author: aikoulou, based on pnikiel
 */

#include <signal.h>

#include <Hdlc/BackendFactory.h>
#include <Sca/Defs.h>
#include <Sca/SynchronousService.h>
#include <Sca/Sca.h>
#include <LogIt.h>
#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <stdlib.h>
#include <stdexcept>
#include <DemonstratorCommons.h>

using namespace ScaSwDemonstrators;

bool exitRequired (false);
void handleSigInt (int)
{
	LOG(Log::INF) << "Caught Ctrl-C, clean shutdown will happen soon...";
	exitRequired = true;
}

int main ()
{
	signal(SIGINT, handleSigInt );

	srand(time(0));
	initializeLogging("INF");

	try
	{
	Sca::Sca sca ("sca-simulator://1") ;

	while (!exitRequired)
	{
		try
		{

		  	uint8_t test_dac_A =rand();
		  	uint8_t test_dac_B =rand();
		  	uint8_t test_dac_C =rand();
		  	uint8_t test_dac_D =rand();
LOG(Log::INF) << "[DAC] writing register A with: "<<static_cast<int>(test_dac_A);
			sca.dac().writeRegister(::Sca::Constants::Dac::DAC_A,test_dac_A);
LOG(Log::INF) << "[DAC] reading register ";
			auto dac_A = sca.dac().readRegister(::Sca::Constants::Dac::DAC_A);
LOG(Log::INF) << "[DAC] expected/obtained :: " << static_cast<int>(test_dac_A) << " / " << static_cast<int>(dac_A) << (static_cast<int>(test_dac_A)==static_cast<int>(dac_A) ? " A OK! " : "");
			

			float voltageA = sca.dac().getVoltageEstimateAtRegister(::Sca::Constants::Dac::DAC_A);
			LOG(Log::INF) << "[DAC] voltage estimate for regA = "<<voltageA;

			sca.dac().writeRegister(::Sca::Constants::Dac::DAC_B,test_dac_B);
			sca.dac().writeRegister(::Sca::Constants::Dac::DAC_C,test_dac_C);
			sca.dac().writeRegister(::Sca::Constants::Dac::DAC_D,test_dac_D);

			auto dac_B = sca.dac().readRegister(::Sca::Constants::Dac::DAC_B);
			auto dac_C = sca.dac().readRegister(::Sca::Constants::Dac::DAC_C);
			auto dac_D = sca.dac().readRegister(::Sca::Constants::Dac::DAC_D);

LOG(Log::INF) << "[DAC] expected/obtained :: " << static_cast<int>(test_dac_B) << " / " << static_cast<int>(dac_B) << (static_cast<int>(test_dac_B)==static_cast<int>(dac_B) ? " B OK! " : "");
LOG(Log::INF) << "[DAC] expected/obtained :: " << static_cast<int>(test_dac_C) << " / " << static_cast<int>(dac_C) << (static_cast<int>(test_dac_C)==static_cast<int>(dac_C) ? " C OK! " : "");
LOG(Log::INF) << "[DAC] expected/obtained :: " << static_cast<int>(test_dac_D) << " / " << static_cast<int>(dac_D) << (static_cast<int>(test_dac_D)==static_cast<int>(dac_D) ? " D OK! " : "");
			

///
/// Experimenting with increment methods:
///

		LOG(Log::INF) << "[DAC] Experimental";
		LOG(Log::INF) << "[DAC] ------------";
		LOG(Log::INF) << "[DAC] Incrementing channel A one step";
		sca.dac().incrementChannelByStep(::Sca::Constants::Dac::DAC_A);
		dac_A = sca.dac().readRegister(::Sca::Constants::Dac::DAC_A);
				
		voltageA = sca.dac().getVoltageEstimateAtRegister(::Sca::Constants::Dac::DAC_A);
		LOG(Log::INF) << "[DAC] voltage estimate for regA = "<<voltageA;


		//10 times ++
		LOG(Log::INF) << "[DAC] Incrementing channel A 10 steps";
		for(int i=0;i<10;i++)
			sca.dac().incrementChannelByStep(::Sca::Constants::Dac::DAC_A);
		dac_A = sca.dac().readRegister(::Sca::Constants::Dac::DAC_A);
		voltageA = sca.dac().getVoltageEstimateAtRegister(::Sca::Constants::Dac::DAC_A);
		LOG(Log::INF) << "[DAC] voltage estimate for regA = "<<voltageA;
		//100 times ++
		LOG(Log::INF) << "[DAC] Incrementing channel A 100 steps";
		for(int i=0;i<100;i++)
			sca.dac().incrementChannelByStep(::Sca::Constants::Dac::DAC_A);
		dac_A = sca.dac().readRegister(::Sca::Constants::Dac::DAC_A);
		voltageA = sca.dac().getVoltageEstimateAtRegister(::Sca::Constants::Dac::DAC_A);
		LOG(Log::INF) << "[DAC] voltage estimate for regA = "<<voltageA;

		//10 times --
		LOG(Log::INF) << "[DAC] Decrementing channel A 10 steps";
		for(int i=0;i<10;i++)
			sca.dac().decrementChannelByStep(::Sca::Constants::Dac::DAC_A);
		dac_A = sca.dac().readRegister(::Sca::Constants::Dac::DAC_A);
		voltageA = sca.dac().getVoltageEstimateAtRegister(::Sca::Constants::Dac::DAC_A);
		LOG(Log::INF) << "[DAC] voltage estimate for regA = "<<voltageA;
		//100 times --
		LOG(Log::INF) << "[DAC] Decrementing channel A 100 steps";
		for(int i=0;i<100;i++)
			sca.dac().incrementChannelByStep(::Sca::Constants::Dac::DAC_A);
		dac_A = sca.dac().readRegister(::Sca::Constants::Dac::DAC_A);
		voltageA = sca.dac().getVoltageEstimateAtRegister(::Sca::Constants::Dac::DAC_A);
		LOG(Log::INF) << "[DAC] voltage estimate for regA = "<<voltageA;


///---------------------
		LOG(Log::INF) << "[DAC] ------------";
		LOG(Log::INF) << "[DAC] ------------";
		LOG(Log::INF) << "[DAC] ------------";
		LOG(Log::INF) << "[DAC] Set voltage on channel A at 0.5V";

		sca.dac().setVoltageOnChannel(::Sca::Constants::Dac::DAC_A,0.5);
		dac_A = sca.dac().readRegister(::Sca::Constants::Dac::DAC_A);
		voltageA = sca.dac().getVoltageEstimateAtRegister(::Sca::Constants::Dac::DAC_A);
		LOG(Log::INF) << "[DAC] voltage estimate read for regA [count] = "<<static_cast<int>(dac_A);
		LOG(Log::INF) << "[DAC] voltage estimate read for regA   [V]   = "<<voltageA;
		

///---------------------
		LOG(Log::INF) << "[DAC] ------------";
		LOG(Log::INF) << "[DAC] Set voltage on channel A at 0.23456789V";

		sca.dac().setVoltageOnChannel(::Sca::Constants::Dac::DAC_A,0.23456789);
		dac_A = sca.dac().readRegister(::Sca::Constants::Dac::DAC_A);
		voltageA = sca.dac().getVoltageEstimateAtRegister(::Sca::Constants::Dac::DAC_A);
		LOG(Log::INF) << "[DAC] voltage estimate read for regA [count] = "<<static_cast<int>(dac_A);
		LOG(Log::INF) << "[DAC] voltage estimate read for regA   [V]   = "<<voltageA;
		
///---------------------
		LOG(Log::INF) << "[DAC] ------------";
		LOG(Log::INF) << "[DAC] Set voltage on channel A at 0.0V";

		sca.dac().setVoltageOnChannel(::Sca::Constants::Dac::DAC_A,0.0);
		dac_A = sca.dac().readRegister(::Sca::Constants::Dac::DAC_A);
		voltageA = sca.dac().getVoltageEstimateAtRegister(::Sca::Constants::Dac::DAC_A);
		LOG(Log::INF) << "[DAC] voltage estimate read for regA [count] = "<<static_cast<int>(dac_A);
		LOG(Log::INF) << "[DAC] voltage estimate read for regA   [V]   = "<<voltageA;
		
///---------------------
		LOG(Log::INF) << "[DAC] ------------";
		LOG(Log::INF) << "[DAC] Set voltage on channel A at 1.0V";

		sca.dac().setVoltageOnChannel(::Sca::Constants::Dac::DAC_A,1.0);
		dac_A = sca.dac().readRegister(::Sca::Constants::Dac::DAC_A);
		voltageA = sca.dac().getVoltageEstimateAtRegister(::Sca::Constants::Dac::DAC_A);
		LOG(Log::INF) << "[DAC] voltage estimate read for regA [count] = "<<static_cast<int>(dac_A);
		LOG(Log::INF) << "[DAC] voltage estimate read for regA   [V]   = "<<voltageA;
		
///---------------------
		LOG(Log::INF) << "[DAC] ------------";
		LOG(Log::INF) << "[DAC] Set voltage on channel A at -0.05V";

		sca.dac().setVoltageOnChannel(::Sca::Constants::Dac::DAC_A,-0.05);
		dac_A = sca.dac().readRegister(::Sca::Constants::Dac::DAC_A);
		voltageA = sca.dac().getVoltageEstimateAtRegister(::Sca::Constants::Dac::DAC_A);
		LOG(Log::INF) << "[DAC] voltage estimate read for regA [count] = "<<static_cast<int>(dac_A);
		LOG(Log::INF) << "[DAC] voltage estimate read for regA   [V]   = "<<voltageA;
		
///---------------------
		LOG(Log::INF) << "[DAC] ------------";
		LOG(Log::INF) << "[DAC] Set voltage on channel A at 2.0V";

		sca.dac().setVoltageOnChannel(::Sca::Constants::Dac::DAC_A,2.0);
		dac_A = sca.dac().readRegister(::Sca::Constants::Dac::DAC_A);
		voltageA = sca.dac().getVoltageEstimateAtRegister(::Sca::Constants::Dac::DAC_A);
		LOG(Log::INF) << "[DAC] voltage estimate read for regA [count] = "<<static_cast<int>(dac_A);
		LOG(Log::INF) << "[DAC] voltage estimate read for regA   [V]   = "<<voltageA;
		

		LOG(Log::INF) << "[DAC] ============= END ITERATION ========================";

		}
		catch (std::exception &e)
		{
			LOG(Log::ERR) << "exception " << e.what();
		}

		usleep(1000000);
	}

    }
    catch(const std::exception& e)
    {
        LOG(Log::ERR) << "Caught: " << e.what() << '\n';
    }




}
