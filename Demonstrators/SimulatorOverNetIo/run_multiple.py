#!/usr/bin/env python

# @author: pnikiel

from __future__ import print_function

import os
import argparse

path_to_simulator = '../../build/Demonstrators/SimulatorOverNetIo/simulator_netio'

parser = argparse.ArgumentParser()
parser.add_argument('--num_scas', type=int, default=1, help='Number of SCAs to simulate')
parser.add_argument('--requests_port_base', type=int, default=29000,
    help='Base TCP/IP for listening for requests, each consecutive SCA will take consecutive ports')
parser.add_argument('--replies_port_base', type=int, default=30000,
    help='Base TCP/IP for listening for replies, each consecutive SCA will take consecutive ports')

args = parser.parse_known_args()

print('Requested to simulate {0} SCAs'.format(args[0].num_scas))

for i in range(0, args[0].num_scas):
    reqs_port = args[0].requests_port_base + i
    reps_port = args[0].replies_port_base + i
    this_args = '--requests_port {0} --replies_port {1} {2}'.format(
        reqs_port,
        reps_port,
        ' '.join(args[1]))
    print('Starting simulator for SCA#{0} with args: {1}'.format(i, this_args))
    os.system('%s %s &' % (path_to_simulator, this_args))
