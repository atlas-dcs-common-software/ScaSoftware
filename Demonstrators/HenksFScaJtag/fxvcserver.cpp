/* Demonstrator Xilinx XVC protocol server for a GBT-SCA JTAG channel */

#include <iostream>
#include <iomanip>
using namespace std;
#include <unistd.h> // For getopt()
#include <ctype.h>  // For toupper()
#include <signal.h> // For sigaction()
#include <string.h> // For strerror()

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <DemonstratorCommons.h>
using namespace ScaSwDemonstrators;

#include <HenksJtag/JtagPort.h>

// Version identifier: year, month, day, release number
const int VERSION_ID = 0x21011100;
// Added performance improvement measures to JtagPort::shift() for XVC protocol,
// in particular to speed up the Vivado memory device programming operation.
//const int VERSION_ID = 0x20090900;
// Added option -p (select an IP port number).

//const int VERSION_ID = 0x20082500;
// First released version.

#define XVC_DEFAULT_PORT 2542
void xvc_server ( HenksJtag::JtagPort &jtagport, int ip_portnr = XVC_DEFAULT_PORT );
void xvc_handler( int fd, HenksJtag::JtagPort &jtagport );
int  sread      ( int fd, void *dest, int len );

void usage();

// ----------------------------------------------------------------------------

void ctrlc_handler( int arg )
{
  throw std::runtime_error( "CTRL-C pressed" );
}

// ----------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
  int      opt;
  int      jtag_rate = 10; // In MHz
  int      group_sz  = 253;
  //string address   = "sca-simulator://1";
  string   address;
  string   trace_lvl = "INF";
  int      ip_portnr = XVC_DEFAULT_PORT;

  // Install a Ctrl-C signal handler
  struct sigaction sa;
  sa.sa_handler = ctrlc_handler;
  sa.sa_flags   = 0;
  sigemptyset( &sa.sa_mask );
  sigaction( SIGINT, &sa, NULL );

  // Parse the options
  while( (opt = getopt(argc, argv, "a:g:hp:R:t:V")) != -1 )
    {
      switch( opt )
        {
        case 'h':
          usage();
          return 0;
        case 'a':
          address = string( optarg );
          break;
        case 'g':
          // Message group size maximum
          if( sscanf( optarg, "%d", &group_sz ) != 1 )
            {
              cout << "### -g: error in argument" << endl;
              usage();
              return 1;
            }
          if( group_sz < 1 || group_sz > 253 )
            {
              cout << "### -g: not in range [1..253]" << endl;
              return 1;
            }
          break;
        case 'p':
          // IP port number to listen on
          if( sscanf( optarg, "%d", &ip_portnr ) != 1 )
            {
              cout << "### -p: error in argument" << endl;
              usage();
              return 1;
            }
          if( ip_portnr < 1 || ip_portnr > 65535 )
            {
              cout << "### -p: not in range [1..65535]" << endl;
              return 1;
            }
          break;
        case 'R':
          // JTAG clock rate (in MHz)
          if( sscanf( optarg, "%d", &jtag_rate ) != 1 )
            {
              cout << "### -R: error in argument" << endl;
              usage();
              return 1;
            }
          if( !(jtag_rate == 20 || jtag_rate == 10 ||
                jtag_rate == 5 || jtag_rate == 4 ||
                jtag_rate == 2 || jtag_rate == 1) )
            {
              cout << "### -R: argument not one of "
                   << "[1,2,4,5,10,20] MHz" << endl;
              return 1;
            }
          break;
        case 't':
          trace_lvl = string( optarg );
          for( auto &c: trace_lvl ) c = toupper( c );
          if( trace_lvl != "ERR" && trace_lvl != "WRN" &&
              trace_lvl != "INF" && trace_lvl != "DBG" &&
              trace_lvl != "TRC" )
            {
              cout << "### -t: trace level should be one of: "
                   << "ERR, WRN, INF, DBG, TRC" << endl;
            }
          break;
        case 'V':
          cout << "Version " << hex << VERSION_ID << dec << endl;
          return 0;
        default: // '?'
          usage();
          return 1;
        }
    }

  if( address.empty() )
    {
      cout << "### Provide a GBT-SCA E-link address (option -a)\n";
      return 1;
    }

  // -------------------------------------------------------------------------

  initializeLogging( trace_lvl );

  try {
    Sca::Sca            sca( address );
    HenksJtag::JtagPort jtagport( sca, group_sz );
    jtagport.configure( jtag_rate );

    xvc_server( jtagport, ip_portnr );
  }
  catch( std::runtime_error &e ) {
    LOG(Log::ERR) << e.what();
    return 1;
  }

  return 0;
}

// ----------------------------------------------------------------------------

void xvc_server( HenksJtag::JtagPort &jtagport, int ip_portnr )
{
  struct sockaddr_in address;

  int s = socket( AF_INET, SOCK_STREAM, 0 );
  if( s < 0 )
    {
      std::string msg( "Failed on socket(): " );
      msg += std::string( strerror(errno) );
      throw std::runtime_error( msg );
    }

  int i = 1;
  setsockopt( s, SOL_SOCKET, SO_REUSEADDR, &i , sizeof(i) );

  // This is an Internet address
  address.sin_family = AF_INET;

  // Let the system figure out our IP address
  address.sin_addr.s_addr = INADDR_ANY;

  // This is the port we will listen on
  address.sin_port = htons( (unsigned short) ip_portnr );

  // Bind: associate the parent socket with a port
  if( bind( s, (struct sockaddr *) &address, sizeof(address) ) < 0 )
    {
      std::string msg( "Failed on bind(): " );
      msg += std::string( strerror(errno) );
      throw std::runtime_error( msg );
    }

  // Listen: make this socket ready to accept connection requests
  if( listen( s, 0 ) < 0 ) // Allow 1 requests to queue up
    {
      std::string msg( "Failed on listen(): " );
      msg += std::string( strerror(errno) );
      throw std::runtime_error( msg );
    }

  /*
  // Simplified server part (Henk, 9 Sep 2020)
  while( true )
    {
      socklen_t nsize = sizeof( address );
      int fd = accept( s, (struct sockaddr *) &address, &nsize );
      if( fd >= 0 )
        {
          uint32_t ipaddr = address.sin_addr.s_addr;
          LOG(Log::INF) << "Connection accepted: fd=" << fd
                        << ", addr="
                        << ((ipaddr>>0) & 0xFF) << "."
                        << ((ipaddr>>8) & 0xFF) << "."
                        << ((ipaddr>>16) & 0xFF) << "."
                        << ((ipaddr>>24) & 0xFF);
        }
      else
        {
          std::string msg( "Failed on accept(): " );
          msg += std::string( strerror(errno) );
          throw std::runtime_error( msg );
        }
      // Connected !
      xvc_handler( fd, jtagport );
      close( fd );
      LOG(Log::INF) << "Connection closed: fd=" << fd;
    }
  */
  fd_set conn;
  int maxfd = 0;

  FD_ZERO( &conn );
  FD_SET( s, &conn );

  maxfd = s;

  while( 1 )
    {
      fd_set read = conn, except = conn;
      if( select( maxfd + 1, &read, 0, &except, 0 ) < 0 )
        {
          std::string msg( "Failed on select(): " );
          msg += std::string( strerror(errno) );
          throw std::runtime_error( msg );
        }

      int fd, newfd;
      for( fd=0; fd<=maxfd; ++fd )
        {
          if( FD_ISSET(fd, &read) )
            {
              if( fd == s )
                {
                  socklen_t nsize = sizeof( address );
                  newfd = accept( s, (struct sockaddr*) &address, &nsize );

                  if( newfd >= 0 )
                    {
                      uint32_t ipaddr = address.sin_addr.s_addr;
                      LOG(Log::INF) << "Connection accepted: fd=" << newfd
                                    << ", addr="
                                    << ((ipaddr>>0) & 0xFF) << "."
                                    << ((ipaddr>>8) & 0xFF) << "."
                                    << ((ipaddr>>16) & 0xFF) << "."
                                    << ((ipaddr>>24) & 0xFF);

                      if( newfd > maxfd )
                        maxfd = newfd;
                      FD_SET( newfd, &conn );
                    }
                  else
                    {
                      std::string msg( "Failed on accept(): " );
                      msg += std::string( strerror(errno) );
                      throw std::runtime_error( msg );
                    }
                }
              else
                {
                  xvc_handler( fd, jtagport );

                  LOG(Log::INF) << "Connection closed: fd=" << fd;
                  close( fd );
                  FD_CLR( fd, &conn );
                }
            }
          else if( FD_ISSET(fd, &except) )
            {
              LOG(Log::INF) << "Connection interrupted: fd=" << fd;
              close( fd );
              FD_CLR( fd, &conn );
              if( fd == s )
                break;
            }
        }
    }
}

// ----------------------------------------------------------------------------

void xvc_handler( int fd, HenksJtag::JtagPort &jtagport )
{
  /*
    The XVC 1.0 communication protocol consists of the following three messages:
      getinfo:
      settck:<period in ns>
      shift:<num bits><tms vector><tdi vector>
    with "getinfo:", "settck:" and "shift:" literal ASCII strings,
    <period in ns> and <num bits> 4-byte integers
    and <tms vector> and <tdi vector> byte arrays, each with a size of
    (<num bits> + 7)/8, so rounded to the next byte.
    For more info see:
    https://www.xilinx.com/support/documentation/application_notes/
            xapp1251-xvc-zynq-petalinux.pdf
  */
  const char xvcInfo[] = "xvcServer_v1.0:2048\n"; // Exact name required!
  int     nbits, nbytes;
  uint8_t data[4096];
  uint8_t reply[4096];

  while( true )
    {
      if( sread( fd, data, 2 ) < 0 )
        return;

      if( memcmp( data, "sh", 2 ) == 0 )
        {
          // Shift command: "shift:"
          if( sread( fd, data, 4 ) < 0 ) // Read "ift:"
            return;
          if( sread( fd, data, 4 ) < 0 )
            return;

          memcpy( &nbits, data, 4 );
          nbytes = (nbits + 7) / 8;

          LOG(Log::DBG) << "XVC shift: nbits=" << nbits;

          // Read 'nbytes' bytes with TMS bits,
          // followed by 'nbytes' bytes with TDO bits
          if( sread( fd, data, nbytes*2 ) < 0 )
            return;

          memset( reply, 0, nbytes ); // Init reply bytes to 0

          //LOG(Log::DBG) << "XVC shift: call JtagPort::shift()";

          uint8_t *tms = &data[0];
          uint8_t *tdo = &data[nbytes];
          uint8_t *tdi = &reply[0];
          jtagport.shift( nbits, tms, tdo, tdi );

          write( fd, reply, nbytes );

          /*
          // DEBUG: display TMS, TDO, TDI bytes
          cout << "TMS= " << hex;
          int j;
          for( j=0; j<nbytes; ++j )
            {
              cout << setw(2) << (unsigned int) tms[j] << " ";
              if( (j & 0x1F) == 0x1F )
                cout << endl << "     ";
            }
          cout << endl;

          cout << "TDO= " << hex;
          for( j=0; j<nbytes; ++j )
            {
              cout << setw(2) << (unsigned int) tdo[j] << " ";
              if( (j & 0x1F) == 0x1F )
                cout << endl << "     ";
            }
          cout << endl;

          cout << "TDI= " << hex;
          for( j=0; j<nbytes; ++j )
            {
              cout << setw(2) << (unsigned int) tdi[j] << " ";
              if( (j & 0x1F) == 0x1F )
                cout << endl << "     ";
            }
          cout << endl;
          */
        }
      else if( memcmp(data, "ge", 2) == 0)
        {
          // Getinfo command: "getinfo:"
          LOG(Log::INF) << "XVC getinfo: reply=" << xvcInfo;

          if( sread(fd, data, 6 ) < 0 ) // Read "tinfo:"
            return;

          write( fd, xvcInfo, sizeof(xvcInfo)-1 );
        }
      else if( memcmp( data, "se", 2 ) == 0 )
        {
          // Settck command: "settck:"
          if( sread(fd, data, 5) < 0 ) // Read "ttck:"
            return;
          if( sread(fd, data, 4) < 0 )
            return;

          LOG(Log::INF) << "XVC settck: " << *((uint32_t *) data);

          write( fd, data, 4 );
        }
    }
}

// ----------------------------------------------------------------------------

int sread( int fd, void *dest, int len )
{
  unsigned char *ptr = (unsigned char*) dest;
  int r;
  while( len > 0 )
    {
      r = read( fd, ptr, len );
      if( r <= 0 )
        return r;
      ptr += r;
      len -= r;
    }
  return 0;
}

// ----------------------------------------------------------------------------

void usage()
{
  cout << "fxvcserver v" << hex << VERSION_ID << dec << endl <<
    "Relays Xilinx XVC protocol JTAG bit streams to and from the JTAG port of a GBT-SCA,\n"
    "for example through its connection (E-link) to a FELIX system.\n"
    "Usage:\n"
    " fxvcserver [-h|V] [-p <portnr>] [-R <rate>] [-g <size>] [-t <lvl>] -a <addr>\n"
    "  -h         : Show this help text.\n"
    "  -V         : Show version.\n"
    "  -p <portnr>: IP port number the server listens on (default: 2542)\n"
    "  -R <rate>  : JTAG clock rate, in MHz, 1,2,4,5,10 or 20 (default: 10).\n"
    "  -g <size>  : Message group size maximum [1..253] (default: 253).\n"
    "  -t <lvl>   : Trace level, one of: ERR, WRN, INF, DBG, TRC.\n"
    "  -a <addr>  : E-link address of the GBT-SCA;\n"
    "               example address: FELIX GBT link #0 EC-channel (E-link number 0x3F)\n"
    "               on host <hostname>:\n"
    "               simple-netio://direct/<hostname>/12340/12350/3f\n\n"
    "In Vivado's Hardware Manager, in the TCL Console type \"connect_hw_server\"\n"
    "(if necessary), followed by \"open_hw_target -xvc_url <ip-address>:<portnr>\"\n"
    "to connect to a Xilinx FPGA connected to the GBT-SCA JTAG port,\n"
    "with <address> and <portnr> the IP address and port number\n"
    "of the host running this fxvcserver.\n";
}

// ----------------------------------------------------------------------------
