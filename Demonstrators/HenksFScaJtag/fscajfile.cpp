/* DEMO program for the JtagOperator class */

#include <fstream>
#include <iostream>
#include <iomanip>
using namespace std;
#include <unistd.h>

#include <DemonstratorCommons.h>
using namespace ScaSwDemonstrators;

#include <HenksJtag/JtagOperator.h>

// Version identifier: year, month, day, release number
const int VERSION_ID = 0x20020300;

void usage();

// ----------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
  int      opt;
  bool     noop      = false;
  int      jtag_rate = 10; // In MHz
  string   address   = "sca-simulator://1";
  string   filename;

  // Parse the options
  while( (opt = getopt(argc, argv, "a:hnR:V")) != -1 )
    {
      switch( opt )
        {
        case 'h':
          usage();
          return 0;
        case 'a':
          address = string( optarg );
          break;
        case 'n':
          noop = true;
          break;
        case 'R':
          // JTAG clock rate (in MHz)
          if( sscanf( optarg, "%d", &jtag_rate ) != 1 )
            {
              cout << "### -R: error in argument" << endl;
              usage();
              return 1;
            }
          if( !(jtag_rate == 20 || jtag_rate == 10 ||
                jtag_rate == 5 || jtag_rate == 4 ||
                jtag_rate == 2 || jtag_rate == 1) )
            {
              cout << "### -R: argument not one of "
                   << "[1,2,4,5,10,20]*1MHz" << endl;
              return 1;
            }
          break;
        case 'V':
          cout << "Version " << hex << VERSION_ID << dec << endl;
          return 0;
        default: // '?'
          usage();
          return 1;
        }
    }

  //if( address.empty() )
  //  {
  //    cout << "### Provide a GBT-SCA E-link address (option -a)\n";
  //    return 1;
  //  }

  // -------------------------------------------------------------------------
  // JTAG bit strings file

  // File name
  if( optind < argc )
    {
      filename = string( argv[optind] );
    }
  else
    {
      cout << "### Filename missing.." << endl;
      exit( 0 );
    }

  initializeLogging( "INF" );

  Sca::Sca sca( address );

  HenksJtag::JtagOperator jtagOperator( sca, jtag_rate );
  if( !filename.empty() )
    {
      if( jtagOperator.setFile( filename ) )
        jtagOperator.displaySequence( true );
    }

  // Only wanted to know what was contained in the strings?
  if( noop )
    return 0;

  // -------------------------------------------------------------------------

  // Ready to upload the sequence?
  if( !jtagOperator.hasValidSequence() )
    {
      LOG(Log::ERR) << "No valid sequence";
      return 1;
    }

  try {
    if( jtagOperator.uploadSequence() )
      LOG(Log::INF) << "JtagOperator::uploadSequence() OKAY";
    else
      LOG(Log::ERR) << "JtagOperator::uploadSequence() failed";

    unsigned int nbytes = jtagOperator.replyBytesSize();
    if( nbytes > 0 )
      {
        // Display returned TDI bits
        uint8_t const *bytes = jtagOperator.replyBytes();
        cout << "Received TDI (" << nbytes << " bytes = "
             << nbytes*8 << " bits):" << endl
             << hex << setfill('0');
        unsigned int i;
        for( i=0; i<nbytes; ++i )
          {
            cout << " " << setw(2) << (unsigned int) bytes[i];
            if( (i & 0xF) == 0xF )
              cout << endl;
          }
        if( (i & 0xF) != 0 )
          cout << endl;
      }
  }
  catch( std::runtime_error &e ) {
    LOG(Log::ERR) << e.what();
    return 1;
  }

  return 0;
}

// ----------------------------------------------------------------------------

void usage()
{
  cout << "fscajfile v" << hex << VERSION_ID << dec << endl <<
    "*** UNDER DEVELOPMENT ***\n"
    "Tool to upload JTAG bit strings from file to the JTAG port of a GBT-SCA.\n"
    "Usage:\n"
    " fscajfile [-h|V] [-n] [-R <rate>] [-a <addr>] <filename>\n"
    "  -h         : Show this help text.\n"
    "  -V         : Show version.\n"
    "  -n         : 'No operation', only read (and display) the JTAG file.\n"
    "  -R <rate>  : JTAG clock rate, in MHz, 1,2,4,5,10 or 20 (default: 10).\n"
    "  -a <addr>  : E-link address of the GBT-SCA\n"
    "               (default: \"sca-simulator://1\")\n"
    " <filename>  : Name of file containing JTAG instructions and data.\n";
}

// ----------------------------------------------------------------------------
