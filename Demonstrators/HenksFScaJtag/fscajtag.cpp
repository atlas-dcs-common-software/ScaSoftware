/* DEMO program for the XilinxJtagProgrammer class */

#include <fstream>
#include <iostream>
#include <iomanip>
#include <functional>
using namespace std;
#include <unistd.h>
#include <string.h>
#include <signal.h> // For sigaction()

#include <HenksJtag/XilinxJtagProgrammer.h>

#include <Hdlc/BackendFactory.h>
#include <boost/program_options.hpp>

//#include <VeryHipEvents/EventRecorder.h>
//using VeryHipEvents::Recorder;

#include <DemonstratorCommons.h>

// ----------------------------------------------------------------------------

struct Config
{
  std::string  address;
  std::string  bitfile;
  bool         debug;
  unsigned int maxGroupSize;
  bool         readId;
  bool         dump;
  unsigned int jtagFrequency;
  unsigned int devicesBefore;
  unsigned int instrBitsBefore;
  unsigned int devicesAfter;
  unsigned int instrBitsAfter;
  unsigned int bypassInstruction;
};

Config parseProgramOptions( int argc, char* argv[] )
{
  using namespace boost::program_options;
  using namespace ScaSwDemonstrators;
  Config config;
  options_description options;
  std::string logLevelStr;
  options.add_options()
    ("help,h",                                                                                     "show help")
    ("address,a",        value<std::string>(&config.address)->default_value("sca-simulator://1"),  helpForAddress )
    ("trace_level,t",    value<std::string>(&logLevelStr)->default_value("INF"),                   "Trace level, one of: ERR,WRN,INF,DBG,TRC")
    ("bitfile",          value<std::string>(&config.bitfile),                                      "Path to the bitfile")
    ("max_group_size,g", value<unsigned int>(&config.maxGroupSize)->default_value(253),            "Max size of a request group, pick from range of [1-253]" )
    ("jtag_freq,j",      value<unsigned int>(&config.jtagFrequency)->default_value(10),            "JTAG TCK frequency, accepted values: 1|2|5|10|20 MHz" )
    ("read_id,i",        bool_switch(&config.readId),                                              "Just read chip ID and exit"  )
    ("dump,d",           bool_switch(&config.dump),                                                "Dump transactions into dump.xml")
    ("devs_before,x",    value<unsigned int>(&config.devicesBefore)->default_value(0),             "Number of devices in JTAG chain before the FPGA" )
    ("bits_before,X",    value<unsigned int>(&config.instrBitsBefore)->default_value(0),           "Total number of instruction bits in JTAG chain before the FPGA" )
    ("devs_after,y",     value<unsigned int>(&config.devicesAfter)->default_value(0),              "Number of devices in JTAG chain after the FPGA" )
    ("bits_after,Y",     value<unsigned int>(&config.instrBitsAfter)->default_value(0),            "Total number of instruction bits in JTAG chain after the FPGA" )
    ("bypass_instr,z",   value<unsigned int>(&config.bypassInstruction)->default_value(0),         "BYPASS instruction for non-FPGA devices in the chain, if non-standard (all ones)" )
    ;
  variables_map vm;
  store( parse_command_line (argc, argv, options), vm );
  notify( vm );
  if( vm.count("help") )
    {
      std::cout << options << std::endl;
      exit(1);
    }
  initializeLogging( logLevelStr );
  if( config.maxGroupSize < 1 || config.maxGroupSize > 253 )
    {
      std::cout << "Max group size out of range, pick a value from 1 (no grouping) up to 253" << std::endl;
      exit(1);
    }

  return config;
}

// ----------------------------------------------------------------------------

class RaiiWrapper
{
public:
  RaiiWrapper(const std::function<void()>& f): m_onDelete(f) {}
  ~RaiiWrapper() { m_onDelete (); }
private:
  std::function<void()> m_onDelete;
};

// ----------------------------------------------------------------------------

void ctrlc_handler( int arg )
{
  throw std::runtime_error( "CTRL-C pressed" );
}

// ----------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
  // Install a Ctrl-C signal handler
  struct sigaction sa;
  sa.sa_handler = ctrlc_handler;
  sa.sa_flags   = 0;
  sigemptyset( &sa.sa_mask );
  sigaction( SIGINT, &sa, NULL );

  Config config = parseProgramOptions( argc, argv );
  if( config.dump )
    {
      //Recorder::initialize(1E6, 1E8);
    }
  //RaiiWrapper dumpOnExit( [config](){
  //    if (config.dump)
  //      {
  //        LOG(Log::INF) << "Hang tight, we're dumping the trace now.";
  //        //Recorder::dump("dump.xml");
  //      }
  //  });
  Sca::Sca sca( config.address );

  try {
    HenksJtag::XilinxJtagProgrammer programmer( sca, config.jtagFrequency, config.maxGroupSize );
    if( config.readId )
      {
        uint32_t id = programmer.readId();
        LOG(Log::INF) << "the ID is: " << std::hex << id;
        LOG(Log::INF) << "which is: " << HenksJtag::XilinxJtagProgrammer::idCodeToString( id );
      }
    else
      {
        programmer.programFromBitFile( config.bitfile,
                                       config.devicesBefore, config.instrBitsBefore,
                                       config.devicesAfter, config.instrBitsAfter,
                                       config.bypassInstruction );
      }
  }
  catch( std::runtime_error &e ) {
    LOG(Log::ERR) << e.what();
    return 1;
  }

  return 0;
}
