/*
 * XilinxJtagProgrammer.h
 *
 *  Created : 18 Jun 2018
 *  Author  : pnikiel
 *  Extended: Dec 2019, henkb
 */

#ifndef INCLUDE_HENKSJTAG_XILINXJTAGPROGRAMMER_H_
#define INCLUDE_HENKSJTAG_XILINXJTAGPROGRAMMER_H_

#include <Sca/Sca.h>
#include <HenksJtag/JtagPort.h>

namespace HenksJtag
{

class BitFile;

class XilinxJtagProgrammer
{
public:
    XilinxJtagProgrammer( Sca::Sca& sca,
                          unsigned int freqMhz = 20,
                          unsigned int maxGroupSize = 240 );
    virtual ~XilinxJtagProgrammer( );

    bool programFromBitFile( const std::string& filename,
                             int devices_before = 0,
                             int instr_bits_before = 0,
                             int devices_after = 0,
                             int instr_bits_after = 0,
                             int bypass_instruction = 0 );

    bool programFromBlob( const std::string& blob,
                          int devices_before = 0,
                          int instr_bits_before = 0,
                          int devices_after = 0,
                          int instr_bits_after = 0,
                          int bypass_instruction = 0 );

    //bool program( BitFile& bitfile );

    bool program( BitFile& bitfile,
                  int devices_before = 0,
                  int instr_bits_before = 0,
                  int devices_after = 0,
                  int instr_bits_after = 0,
                  int bypass_instruction = 0 );

    //uint32_t readId( );

    uint32_t readId( int devices_before = 0,
                     int instr_bits_before = 0,
                     int devices_after = 0,
                     int instr_bits_after = 0,
                     int bypass_instruction = 0 );

    static std::string idCodeToString( uint32_t id );

private:
    HenksJtag::JtagPort m_jtagPort;
    unsigned int        m_freqMhz;
};

} /* namespace HenksJtag */

#endif /* INCLUDE_HENKSJTAG_XILINXJTAGPROGRAMMER_H_ */
