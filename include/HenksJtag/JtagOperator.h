/* -------------------------------------------------------------------------
File   : JtagOperator.h

Descr  : Class managing a JTAG 'sequence', i.e. (pairs of) instruction and
         data bit strings for upload into a GBT-SCA JTAG channel
         with optional TDI read-back on a per-string basis.
         The sequence can be read from a file and/or can be composed by
         adding instruction or data bit strings one by one.

History: 29JAN20; Created.
         06JUL21: Update of uploadSequence() to use the new JtagPort calls
                  with integrated TDI readback (which were added to support
                  JTAG from OPC-UA), when required.
---------------------------------------------------------------------------- */

#ifndef JTAGOPERATOR_H
#define JTAGOPERATOR_H

#include <Sca/Sca.h>
#include <HenksJtag/JtagPort.h>
#include <HenksJtag/JtagSequence.h>

namespace HenksJtag
{
  class JtagOperator
  {
  public:
    JtagOperator( Sca::Sca&    sca,
                  unsigned int freqMhz = 20,
                  unsigned int maxGroupSize = 240 );
    virtual ~JtagOperator( );

    bool setFile( std::string filename );

    bool addString( std::vector<unsigned char> &data,
                    unsigned int nbits,
                    bool instr, bool read = false );

    bool hasValidSequence( );
    void clearSequence( );
    unsigned int sequenceSize( ) const;

    bool uploadSequence( );
    bool uploadSequence_old( );

    uint8_t const *replyBytes( );
    unsigned int replyBytesSize( ) const { return m_replyBytes.size(); }

    void displaySequence( bool all = true );

  private:
    HenksJtag::JtagPort     m_jtagPort;
    HenksJtag::JtagSequence m_jtagSequence;
    std::vector<uint8_t>    m_replyBytes;
  };
}
// ----------------------------------------------------------------------------
#endif /* JTAGOPERATOR_H */
