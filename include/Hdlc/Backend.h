
#ifndef BACKEND_H
#define BACKEND_H

#include <Hdlc/Payload.h>

#include <functional>
#include <string>
#include <vector>

#include <stdint.h>

namespace Hdlc {

enum HdlcControlCodes
{
    CONNECT = 0x2F,
    RESET   = 0x8F,
	TEST	= 0xE3
};

struct BackendStatistics
/** This structure groups statistics output together. */
{
    uint64_t numberFramesSent;
    uint64_t numberFramesReceived;
    uint64_t lastReplySecondsAgo;
    // TODO: time of last error and time of last reply
};

class Backend
{
public:

	typedef std::function<void(const Hdlc::Payload&)> ReceiveCallBack;

	virtual ~Backend();

  /**
   * @param  request
   * @throws when-fail
   *
   */
  virtual void send (const Payload &request ) = 0;

  virtual void send (
          const std::vector<Payload> &requests,
          const std::vector<unsigned int> times
      ) = 0;

  virtual void sendHdlcControl (uint8_t hdlcControl) = 0;

  /**
   * @param  callback
   */
  virtual void subscribeReceiver ( ReceiveCallBack f ) = 0;
  
  virtual void unSubscribeReceiver ( ) = 0;


  /**
   * @return std::string
   */
  virtual const std::string& getAddress ( ) const = 0;


  virtual BackendStatistics getStatistics () const = 0;


};
} // end of package namespace

#endif // BACKEND_H
