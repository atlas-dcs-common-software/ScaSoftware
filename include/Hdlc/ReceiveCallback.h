
#ifndef RECEIVECALLBACK_H
#define RECEIVECALLBACK_H

#include <string>

namespace Hdlc {

class ReceiveCallback
{
public:



  /**
   * @param  reply
   */
  virtual void onReceive (Hdlc::Payload reply ) = 0;

};
}; // end of package namespace

#endif // RECEIVECALLBACK_H
