/*
 * NetioSendSocketsFactory.h
 *
 *  Created on: 25 Nov 2019
 *      Author: pnikiel
 *
 * The primary motivation of this class is to enable
 * sharing of the same netio socket (now send_socket, in
 * future also subscribe sockets), among multiple connections
 * to SCAs which are served by the same felixcore instance
 */

#ifndef INCLUDE_NETIOSIMPLEBACKEND_NETIOSOCKETSFACTORY_H_
#define INCLUDE_NETIOSIMPLEBACKEND_NETIOSOCKETSFACTORY_H_

#include <list>
#include <mutex>

#include <netio.hpp>

namespace NetIoSimpleBackend
{

class HdlcBackend;

class NetioSendSocketsFactory
{
public:
    static netio::low_latency_send_socket& requestSendSocket (
            const HdlcBackend* requestorId,
            const std::string& felixcoreHostName,
            unsigned int portToSca);

    static void releaseSendSocket (
            const HdlcBackend* requestorId);

private:
    struct ConnectionId
    /** ConnectionId is a unique identifier of every multiplexed connection between felixcore and SCA-SW,
        as of now we identify it by felixcore hostname and port */
    {
        ConnectionId (
                const std::string& aHostName,
                unsigned int aPort):
                    felixcoreHostName(aHostName), portToSca (aPort) {}
        bool operator==(const ConnectionId& other) const { return other.felixcoreHostName == felixcoreHostName && other.portToSca == portToSca;}
        const std::string felixcoreHostName;
        const unsigned int portToSca;
        std::string toString() const { return "<netio::send_socket for felixcore="+felixcoreHostName+" port="+std::to_string(portToSca)+">"; }
    };

    struct SendSocketContainer
    /** SendSocketContainer contains the actual send socket which is reused by potentially multiple SCAs. */
    {
        SendSocketContainer (const ConnectionId & p) :
            connectionId(p),
            socket(&s_netioContext) {}
        const ConnectionId connectionId;
        netio::low_latency_send_socket socket;
        std::list<const HdlcBackend*> users;
    };

    static void ensureEventLoopStarted();
    static void ensureEventLoopStopped();

    static std::list<SendSocketContainer> s_factoryState;

    static netio::context s_netioContext;

    static std::thread s_netioContextEventLoopThread;

    static std::mutex s_accessLock;
};

}



#endif /* INCLUDE_NETIOSIMPLEBACKEND_NETIOSOCKETSFACTORY_H_ */
