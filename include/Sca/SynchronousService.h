/*
 * SynchronousService.h
 *
 *  Created on: May 9, 2016
 *      Author: pnikiel
 *
 * SynchronousService is built on top of HDLC Backend and it is specific to communication with the SCA.
 * It provides synchronous communication with the SCA, that is: send request and wait for reply, atomically.
 *
 * The reason that SynchronousService is specific to the SCA is that it is SCA features like transaction id and
 * channel id that let associate replies to requests.
 *
 */

#ifndef SCA_SYNCHRONOUSSERVICE_H_
#define SCA_SYNCHRONOUSSERVICE_H_

#include <Sca/Request.h>
#include <Sca/Reply.h>
#include <Sca/TransactionIdCoordinator.h>
#include <Sca/AwaitedRepliesController.h>

#include <Hdlc/Payload.h>

#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>

#include <map>
#include <vector>
#include <atomic>

// forward-decl
namespace Hdlc
{
class Backend;
}

namespace Sca
{



class SynchronousService
{
	//! The functions for class users:
	//! - registerChannel for initialization
	//! - sendAndWaitReply for usage of GBT-SCA

	class SynchronousChannel
	{

	public:
		SynchronousChannel(
				Hdlc::Backend * backend,
				SynchronousService& parent):
					m_backend(backend),
					m_synchronousService(parent)
	        {}
		Reply sendAndWaitReply (Request &request, unsigned int timeoutMs );
		std::vector<Reply> sendAndWaitReply (std::vector<Request> &requests, unsigned int timeoutMs );

		void onReceive( const ::Hdlc::Payload& reply );

	private:
		boost::timed_mutex m_oneUserPerChannelLock;
		Hdlc::Backend * m_backend;
		boost::mutex m_condVarLock;
		boost::condition_variable m_condVar;
		TransactionIdCoordinator m_tidCoordinator;
	    AwaitedRepliesController m_awaitedRepliesController;
	    SynchronousService& m_synchronousService;
	};

public:
	SynchronousService( ::Hdlc::Backend * backend );
	~SynchronousService ();

	void registerChannel( unsigned char channelId );
	void onReceive( const ::Hdlc::Payload& reply );

	//! Request frame will get transaction id appended
	Reply sendAndWaitReply (Request &request, unsigned int timeoutMs=1000 );
	std::vector<Reply> sendAndWaitReply (std::vector<Request> &requests, unsigned int timeoutMs=1000 );

	//! Call to maintain statistics of lost replies
	void onLostReplies (uint64_t adder) { m_lostRepliesCounter.fetch_add(adder); }
	uint64_t lostReplies() const { return m_lostRepliesCounter.load(); }

private:
	::Hdlc::Backend * m_backend;  // this backend will serve us
	std::map<unsigned char, std::unique_ptr<SynchronousChannel>> m_channelMap;
	std::atomic_uint_fast64_t m_lostRepliesCounter;
};

}

#endif /* SCA_SYNCHRONOUSSERVICE_H_ */
