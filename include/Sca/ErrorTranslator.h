/*
 * ErrorTranslator.h
 *
 *  Created on: Mar 22, 2017
 *      Author: pnikiel
 */

#ifndef INCLUDE_SCA_ERRORTRANSLATOR_H_
#define INCLUDE_SCA_ERRORTRANSLATOR_H_

#include <string>
#include <Sca/Defs.h>

namespace Sca
{

	std::string translateError( Constants::Errors e );


}


#endif /* INCLUDE_SCA_ERRORTRANSLATOR_H_ */
