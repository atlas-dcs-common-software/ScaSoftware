/*
 * TransactionIdCoordinator.h
 *
 *  Created on: May 10, 2016
 *      Author: pnikiel
 */

#ifndef SCA_TRANSACTIONIDCOORDINATOR_H_
#define SCA_TRANSACTIONIDCOORDINATOR_H_

#include <boost/thread/mutex.hpp>

#include <vector>
#include <Sca/Defs.h>

namespace Sca
{

class TransactionIdCoordinator
{
public:
	TransactionIdCoordinator(unsigned int numAvailable=::Sca::Constants::Specs::TRANSACTION_IDS_AVAILABLE, 
								unsigned int firstTid=::Sca::Constants::Specs::TRANSACTION_ID_FIRST):
		m_tidBitmap( numAvailable, false ),
		m_numAvailableTids( numAvailable ),
		m_firstTid( firstTid ),
		m_startSearchWith( firstTid )
{}

	bool allocateTid (unsigned int * output);
	void freeTid (unsigned int tid);


private:
	bool searchInRange(unsigned int from, unsigned int to, unsigned int* output);

	std::vector<bool> m_tidBitmap; // false at given position means that this tid is free
	unsigned int m_numAvailableTids;
	unsigned int m_firstTid;
	unsigned int m_startSearchWith;
	boost::mutex m_uniqueAccessMutex;

};

}

#endif /* SCA_TRANSACTIONIDCOORDINATOR_H_ */
