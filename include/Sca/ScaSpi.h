/*
 * ScaSpi.h
 *
 *  Created on: Dec 3, 2018
 *      Author: Paris Moschovakos
 */

#pragma once

namespace Sca
{

class Spi
	{
	public:
		Spi ( SynchronousService& ss): m_synchronousService( ss ) {}

		void configureBus ( unsigned int transmissionBaudRate,
				uint8_t transmissionBitLength,
				bool sampleAtFallingTxEdge,
				bool sampleAtFallingRxEdge,
				bool lsbToMsb,
				bool sclkIdleHigh,
				bool autoSsMode );

		void writeSlave ( int selectedSlave, std::vector<uint8_t> &payload, bool toggleSS = true );
		void writeSlaves ( uint8_t selectedSlaves, std::vector <uint8_t> &payload, bool toggleSS = true );
		std::vector<uint8_t> getSlaveReply ( uint8_t selectedSlave, bool toggleSS );
		std::vector<uint8_t> getSlaveReplies ( int numberOfChunks, uint8_t selectedSlave, bool toggleSS = true );

		void setSelectedSlave ( int slaveId );
		void setSelectedSlaves ( uint8_t slaveIdMask );
		void resetSelectedSlaves ();
		uint8_t getSelectedSlaves ();

		Reply getDataRegister( uint8_t miso );
		Reply setDataRegister( uint8_t mosi, std::vector<uint8_t>::iterator &dataIter );

		void setTransmissionBaudRate ( unsigned int transmissionBaudRate );
		float getTransmissionBaudRate ();

		/* allowed range 1-128 bits */
		void setTransmisionBitLength ( unsigned int transmissionBitLength );
		uint8_t getTransmisionBitLength ();

		bool getInvSclk ();
		void setInvSclk ( bool setSclkInv );

		bool getRxEdge ();
		void setRxEdge ( bool setRxEdge );

		bool getTxEdge ();
		void setTxEdge ( bool setTxEdge );

		bool getLsbToMsb ();
		void setLsbToMsb ( bool setMsbToLsb );

		bool getSsMode ();
		void setSsMode ( bool setSsMode );

		void setSpiMode ( unsigned int spiMode );
		unsigned int getSpiMode ();

	private:

		uint16_t getControlRegister();
		void writeSpiSlaveChunk ( std::vector<uint8_t>::iterator &dataIter );
		unsigned int getSpiIterations ( std::vector<uint8_t> &payload );
		SynchronousService& m_synchronousService;
		void go();
		// Initial values of an SCA when powered on
		float m_baudRate = 20000000.;
		unsigned int m_transmissionBitLength = 96;
		bool m_setSclkInv = 0;
		bool m_setGoFlag = 0;
		bool m_setRxEdge = 0;
		bool m_setTxEdge = 0;
		bool m_setMsbToLsb = 0;
		bool m_setSsMode = 1;
		uint8_t m_spiMode = 0;

	};

}
