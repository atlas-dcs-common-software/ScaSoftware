#pragma once

#include <memory>
#include <list>
#include <mutex>
#include <condition_variable>

#include <Hdlc/Backend.h>
#include <Hdlc/Payload.h>
#include <Hdlc/LocalStatistician.h>
#include <NetioNextBackend/NetioNextAddress.h>

#include <hdlc_coder/hdlc.hpp>
#include <felix/felix_client_thread_impl.hpp>

namespace NetioNextBackend
{

class HdlcBackend : public Hdlc::Backend
{
public:
	HdlcBackend (const std::string& netioNextAddress);
	virtual ~HdlcBackend ();

	void send (const Hdlc::Payload &request ) final;
    void send (
              const std::vector<Hdlc::Payload> &requests,
              const std::vector<unsigned int> times
          ) final;

	void sendHdlcControl (uint8_t hdlcControl) final;
	void cleanUp ();

	void on_init();
	void on_connect(uint64_t fid);
    void on_disconnect(uint64_t fid);
	void on_data(uint64_t fid, const uint8_t* data, size_t size, uint8_t status);

	void subscribeReceiver ( ReceiveCallBack f ) final { m_receiver = f; }
	void unSubscribeReceiver() final;

	const std::string& getAddress () const final { return m_originalNetioNextAddress; }
	virtual Hdlc::BackendStatistics getStatistics () const { return m_statistician.toBackendStatistics(); }

private:
	std::string m_originalNetioNextAddress;
	std::shared_ptr<const NetioNextAddress> m_netioNextAddress;
	ReceiveCallBack m_receiver;
	Hdlc::LocalStatistician m_statistician;

	unsigned int m_seqnr; // HDLC sequence number

    bool m_clientSubscribed;
	std::mutex m_subscribeSynchronizationMutex;
	std::condition_variable m_subscribedConditionVariable;

	bool m_clientSendDataInitialized;
	std::mutex m_initSendDataSynchronizationMutex;
	std::condition_variable m_sendDataInitializedConditionVariable;

	std::mutex m_transmitSynchronizationMutex;

	FelixClientThreadImpl::OnDataCallback m_felixConfigOnData;
	FelixClientThreadImpl::OnInitCallback m_felixConfigOnInit;
	FelixClientThreadImpl::OnConnectCallback m_felixConfigOnConnect;
	FelixClientThreadImpl::OnDisconnectCallback m_felixConfigOnDisconnect;
	FelixClientThreadImpl::Properties m_felixConfigProperties;
	FelixClientThreadImpl::Config m_felixConfig;
	FelixClientThreadImpl m_felixClient;

	inline void printFelixClientProperties();

};


}
