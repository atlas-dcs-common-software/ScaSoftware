/*
 * 	NetioNextAddress.h
 *
 *  Created on: Jan 22, 2021
 *      Author: Paris Moschovakos
 */

#pragma once

#include <string>

namespace NetioNextBackend
{

class NetioNextAddress
{
public:
	NetioNextAddress (const std::string& stringAddress);

	std::string toString() const { return m_originalAddress; }

	uint64_t fidTx() const { return m_fidTx; }
	uint64_t fidRx() const { return m_fidRx; }

private:
	std::string m_originalAddress;
	uint64_t m_fidTx;
	uint64_t m_fidRx;

};

}