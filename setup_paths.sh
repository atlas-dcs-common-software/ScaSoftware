# This script must be sourced and NOT executed. 
# For instance do:
# 
#   source setup_paths.sh

# Don't forget to also source setup.sh from your felix installation directory

# Description:
#
# This script resolves SCA-SW dependencies from a given LCG release
# and for a specific platform, in order to facilitate the integration 
# with TDAQ software (netio, netio-next felixbase and hdlc_coder)
#
# Authors: Paris

PLATFORM_TAG="x86_64-el9"

usage()
{
	echo
	echo "Usage"
	echo
	echo "source setup_paths.sh"
	echo
}

epilogue()
{
	printf '%21s %s \n' \
        "PLATFORM:" "[${PLATFORM_TAG}]" \
		"LCG:" "[${PATH_TO_LCG}/releases/${LCG_VERSION}]" \
		"c++ (GCC):" "[${PATH_TO_LCG}/releases/gcc/${GCC_VERSION}/${X_BINARY_TAG}]" \
		"CMake:" "[${PATH_TO_LCG}/releases/${LCG_VERSION}/CMake/${CMAKE_VERSION}/${X_BINARY_TAG}]" \
		"BOOST_ROOT:" "[${BOOST_ROOT}]" \
		"PROTOBUF_HEADERS:" "[${PROTOBUF_HEADERS}]" \
		"PROTOBUF_DIRECTORIES:" "[${PROTOBUF_DIRECTORIES}]" \
		"PROTOBUF_LIBS:" "[${PROTOBUF_LIBS}]" \
		"FELIX_ROOT:" "[${FELIX_ROOT}]"
}

isP1() {
    IP_FIELD1=`hostname -I | cut -d '.' -f 1`
    if [ "$IP_FIELD1" = "10" ]; then
        echo "Info: We are in P1"
        return 0
    else
		echo "Info: We are in GPN"
        return 1
    fi
}

if isP1; then
	PATH_TO_LCG="/sw/atlas/sw/lcg"
	PATH_TO_FELIX="/sw/atlas/felix"
else
	PATH_TO_LCG="/cvmfs/sft.cern.ch/lcg"
	PATH_TO_FELIX="/cvmfs/atlas-online-nightlies.cern.ch/felix/releases"
fi

LCG_VERSION="LCG_104b"
PLATFORM="el9"
COMPILER="gcc13"
GCC_VERSION="13.1.0"
CMAKE_VERSION="3.26.2"
BOOST_VERSION="1.82.0"
LCG_VERSION_PROTOBUF="LCG_101"
PROTOBUF_VERSION="2.5.0"

# Post common for both platforms
FELIX_VERSION="felix-04-02-12-rm4-stand-alone"
X_BINARY_TAG_SHORT="x86_64-${PLATFORM}"
X_BINARY_TAG="${X_BINARY_TAG_SHORT}-${COMPILER}-opt"

if [ "$1" == "-h" ]
then
    usage
else

    #  First the compiler
    if [ -e ${PATH_TO_LCG}/releases/gcc/${GCC_VERSION}/${X_BINARY_TAG_SHORT}/setup.sh ]; then
        source ${PATH_TO_LCG}/releases/gcc/${GCC_VERSION}/${X_BINARY_TAG_SHORT}/setup.sh
    else
        echo "Error: Cannot access ${PATH_TO_LCG}/releases/gcc/${GCC_VERSION}/${X_BINARY_TAG_SHORT}/setup.sh"
    fi
    #  Then CMake
    if [ -e ${PATH_TO_LCG}/releases/${LCG_VERSION}/CMake/${CMAKE_VERSION}/${X_BINARY_TAG}/CMake-env.sh ]; then
        export PATH=${PATH_TO_LCG}/releases/${LCG_VERSION}/CMake/${CMAKE_VERSION}/${X_BINARY_TAG}/bin:$PATH
    else
        echo "Error: Cannot access ${PATH_TO_LCG}/releases/${LCG_VERSION}/CMake/${CMAKE_VERSION}/${X_BINARY_TAG}/CMake-env.sh"
    fi
    #  Then Boost
    export BOOST_ROOT="${PATH_TO_LCG}/releases/${LCG_VERSION}/Boost/${BOOST_VERSION}/${X_BINARY_TAG}"
    #  Then protobuf
    export PROTOBUF_HEADERS="${PATH_TO_LCG}/releases/${LCG_VERSION_PROTOBUF}/protobuf/${PROTOBUF_VERSION}/x86_64-centos7-gcc11-opt/include"
    export PROTOBUF_DIRECTORIES="${PATH_TO_LCG}/releases/${LCG_VERSION_PROTOBUF}/protobuf/${PROTOBUF_VERSION}/x86_64-centos7-gcc11-opt/lib"
    export PROTOBUF_LIBS="-lprotobuf"
    # Then felix
    if [ -z "${FELIX_ROOT}" ]; then
        if [ -e "${PATH_TO_FELIX}/${FELIX_VERSION}/${X_BINARY_TAG}" ]; then
            export FELIX_ROOT="${PATH_TO_FELIX}/${FELIX_VERSION}/${X_BINARY_TAG}"
        else
            echo "Error: Cannot access ${PATH_TO_FELIX}/${FELIX_VERSION}/${X_BINARY_TAG}"
        fi
    else
        echo "FELIX_ROOT is already set. Using that..."
    fi
    epilogue
fi
