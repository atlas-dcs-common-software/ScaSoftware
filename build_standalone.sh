#!/usr/bin/env bash
#
# Date: 13/09/2021
# Paris

source setup_paths.sh $1

if [ -z "$FELIX_ROOT" ]
then
      echo You need to provide a valid FELIX_ROOT which should contain FELIX dependancies.
      exit 1
else
      echo Your current FELIX_ROOT is ${FELIX_ROOT}
fi

if [ ! -z "$PYTHONPATH" ]
then
      echo PYTHONPATH found to not be empty. Cleaning up...
      unset PYTHONPATH
fi

rm -Rf build
mkdir build
cd build
cmake ../ -DSTANDALONE_SCA_SOFTWARE=1 -DHAVE_NETIO_NEXT=1 -DHAVE_NETIO=1 -DSTANDALONE_WITH_EVENT_RECORDER=OFF -DCMAKE_BUILD_TYPE=Debug
NUM_JOBS=$((3*`nproc`/2)) # 1.5 x #proc
make -j $NUM_JOBS || exit 1
cd ..
echo "Built standalone ScaSoftware with netio-next"
