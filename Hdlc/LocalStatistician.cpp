/*
 * LocalStatistician.cpp
 *
 *  Created on: 1 Aug 2017
 *      Author: pnikiel
 */

#include <Hdlc/LocalStatistician.h>

#include <chrono>

namespace Hdlc
{

LocalStatistician::LocalStatistician():
        m_numberFramesSent(0),
        m_numberFramesReceived(0)
{
}

BackendStatistics LocalStatistician::toBackendStatistics () const
{
    BackendStatistics bs;
    bs.numberFramesReceived = m_numberFramesReceived.load();
    bs.numberFramesSent = m_numberFramesSent.load();
    uint64_t ts =  std::chrono::duration_cast<std::chrono::seconds> (std::chrono::system_clock::now().time_since_epoch()).count();
    bs.lastReplySecondsAgo = ts - m_lastReplyTimeStamp;
    return bs;

}

void LocalStatistician::onFrameReceived()
{
    m_numberFramesReceived.fetch_add(1);
    uint64_t ts =  std::chrono::duration_cast<std::chrono::seconds> (std::chrono::system_clock::now().time_since_epoch()).count();
    m_lastReplyTimeStamp = ts;
}

}

